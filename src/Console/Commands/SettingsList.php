<?php

namespace KevinKao\Lipton\Console\Commands;

use Illuminate\Console\Command;
use KevinKao\Lipton\Models\Settings;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Output\StreamOutput;

class SettingsList extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'settings:list';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "List settings";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $settings = Settings::all();
        $output = new StreamOutput(fopen('php://stdout', 'w'));
        $table = new Table($output);
        $table->setHeaders(['display', 'key', 'value']);
        foreach($settings as $setting) {
            $table->addRow([__($setting->display_name), $setting->key, $setting->value]);
        }
        $table->render();
    }
}
