<?php

namespace KevinKao\Lipton\Console\Commands;

use Illuminate\Console\Command;
use KevinKao\Lipton\Models\CmsCategory;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Output\StreamOutput;

class CategoryList extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'category:list';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "List all cms category";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $categories = CmsCategory::all();
        $output = new StreamOutput(fopen('php://stdout', 'w'));
        $table = new Table($output);
        $table->setHeaders(['id', 'title']);
        foreach($categories as $category) {
            $table->addRow([$category->id, $category->title]);
        }
        $table->render();
    }
}
