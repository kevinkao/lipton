<?php

namespace KevinKao\Lipton\Console\Commands;

use Illuminate\Console\Command;
use KevinKao\Lipton\Models\CmsCategory;
use KevinKao\Lipton\Models\CmsCrawler;

class CrawlerCategory extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crawler:category {--crawler=crawler} {--category=category}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Update crawler's category";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $crawler = CmsCrawler::find($this->option('crawler'));
        if (empty($crawler)) {
            $this->error("No such crawler.");
            return;
        }
        $category = CmsCategory::find($this->option('category'));
        if (empty($category)) {
            $this->error("No such category.");
            return;
        }
        try {
            $crawler->cms_category_id = $this->option('category');
            $crawler->save();
            $this->info("Update {$crawler->title}'s category to {$category->title} ok");
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }
    }
}
