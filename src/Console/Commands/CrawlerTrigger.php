<?php

namespace KevinKao\Lipton\Console\Commands;

use Illuminate\Console\Command;
use KevinKao\Lipton\Models\CmsCrawler;

class CrawlerTrigger extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crawler:trigger {--crawler=crawler}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Trigger specify crawler';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $crawler = CmsCrawler::find($this->option('crawler'));
        if (empty($crawler)) {
            $this->error("No such crawler");
            return;
        }
        try {
            $crawler->command_status = 0;
            $crawler->save();
            $this->info("You must then execute the following commands.");
            $this->info("php artisan schedule:run");
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }
    }
}
