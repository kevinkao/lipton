<?php

namespace KevinKao\Lipton\Console\Commands;

use Illuminate\Console\Command;
use KevinKao\Lipton\Models\Settings;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Output\StreamOutput;

class SettingsUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'settings:update {--key=} {--value=} {--type=text}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Update settings";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $key = $this->option('key');
        $value = $this->option('value');
        $type = $this->option('type');
        $setting = Settings::where('key', $key)->first();

        try {
            if (empty($setting)) {
                if ($this->confirm("No such setting key: {$key}, value: {$value}, do you want to add it?")) {
                    $setting = new Settings();
                    $setting->key = $key;
                    $setting->value = $value;
                    $setting->type = isset($type) ? $type : "text";
                    $setting->save();
                }
            } else {
                $setting->value = $value;
                $setting->save();
            }
            $this->info("Settings updated.");
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }
    }
}
