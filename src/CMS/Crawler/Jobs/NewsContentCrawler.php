<?php

namespace KevinKao\Lipton\CMS\Crawler\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use KevinKao\Lipton\Constants;
use KevinKao\Lipton\CMS\UserAgent;
use KevinKao\Lipton\Models\CmsCrawler;
use QL\QueryList;

use Log;

class NewsContentCrawler implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $crawler;
    protected $url;
    protected $middlewareGroup;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(CmsCrawler $crawler, $url)
    {
        $this->crawler = $crawler;
        $this->url = $url;
        $this->middlewareGroup = config('lipton.crawler.middleware.content');
        Log::channel('crawler')->debug("{$this->url} crawling");
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $rules = [];
            foreach($this->crawler->contentRule as $item) {
                $rules[$item->column] = [
                    $item->selector,
                    $item->type,
                    $item->filter,
                ];
            }

            $ql = QueryList::get($this->url, [], [
                        'headers' => [
                            'User-Agent' => UserAgent::random()
                        ]
                    ])
                    ->rules($rules);

            if ($this->crawler->charset === Constants::CMS_CRAWLER_CHARSET_GB2312) {
                $ql->encoding('UTF-8', 'GB2312')->removeHead();
            }

            $data = $ql->query()
                    ->getData(function($data) use ($rules) {
                        $tmp = $data;
                        if (array_key_exists('content', $rules)) {
                            foreach($this->middlewareGroup as $class) {
                                $middleware = app()->make($class);
                                $tmp = $middleware->handle($tmp, $this->crawler, $this->url);
                                if ($tmp === false) {
                                    break;
                                }
                            }
                        }
                        return $tmp;
                    })
                    ->all();

            Log::channel('crawler')->debug("{$this->url} finish");
        } catch (\Exception $e) {
            Log::channel('crawler')->error($e->getMessage());
        }
    }
}
