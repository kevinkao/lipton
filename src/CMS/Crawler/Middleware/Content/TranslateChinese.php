<?php

namespace KevinKao\Lipton\CMS\Crawler\Middleware\Content;

use Illuminate\Support\Facades\Storage;
use Log;

class TranslateChinese
{
    public function handle($data, $crawler, $url)
    {
        if ($crawler->translate == 0) {
            return $data;
        }

        $filename = 'translate' . (int)(microtime(true) * 1000);
        $infile = $filename . '-in';
        $outfile = $filename . '-out';

        $data['title'] = $this->translateChinese($data['title'], $infile, $outfile);
        $data['content'] = $this->translateChinese($data['content'], $infile, $outfile);

        Storage::delete($infile);
        Storage::delete($outfile);

        return $data;
    }

    private function translateChinese($input, $infile, $outfile)
    {
        if (Storage::put($infile, $input) && Storage::put($outfile, '')) {
            $command = sprintf("/usr/bin/opencc -c t2s -i %s -o %s", Storage::path($infile), Storage::path($outfile));
            exec($command, $output, $reval);
            if ($reval > 0) {
                Log::channel('crawler')->debug("[opencc] translate fail.");
                Log::channel('crawler')->debug($output);
                return $input;
            }
            return Storage::get($outfile);
        }
        Log::channel('crawler')->debug("[opencc] write content fail.");
        return $input;
    }
}
