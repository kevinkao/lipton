<?php

namespace KevinKao\Lipton\CMS;

class Frequency
{
    const EVERY_MINUTE         = 0;
    const EVERY_FIVE_MINUTE    = 1;
    const EVERY_TEN_MINUTE     = 2;
    const EVERY_FIFTEEN_MINUTE = 3;
    const EVERY_THIRTY_MINUTE  = 4;
    const HOURLY               = 5;
    const EVERY_TWO_HOURS      = 6;
    const EVERY_THREE_HOURS    = 7;
    const EVERY_FOUR_HOURS     = 8;
    const DAILY                = 9;
    const WEEKLY               = 10;
    const MONTHLY              = 11;
}