<?php

namespace KevinKao\Lipton\Models;

use Illuminate\Database\Eloquent\Model;
use Storage;

class CmsMedia extends Model
{
    protected $table = 'cms_media';

    public function deleteFile()
    {
        return Storage::disk('public')->delete($this->path);
    }
}