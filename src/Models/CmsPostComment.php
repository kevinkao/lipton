<?php

namespace KevinKao\Lipton\Models;

use Illuminate\Database\Eloquent\Model;

class CmsPostComment extends Model
{
    protected $table = 'cms_post_comment';
    protected $fillable = ['title', 'content'];

    public function post()
    {
        return $this->belongsTo(CmsPost::class, 'cms_post_id', 'id');
    }

    public function author()
    {
        return $this->belongsTo(User::class, 'author_id', 'id');
    }

    public function retrieveContent($len = 12)
    {
        $content = strip_tags($this->content);
        $content = str_replace(' ', '', $content);
        $content = preg_replace('/[\s\t]+/', '', $content);

        if (mb_strlen($content) > $len) {
            return mb_substr($content, 0, $len - 1) . '...';
        }
        return $content;
    }
}