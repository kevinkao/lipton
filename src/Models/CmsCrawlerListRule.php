<?php

namespace KevinKao\Lipton\Models;

use Illuminate\Database\Eloquent\Model;

class CmsCrawlerListRule extends Model
{
    protected $table = 'cms_crawler_list_rule';
    protected $fillable = ['crawler_id', 'limit', 'start_url', 'selector'];
    public $timestamps = false;
}