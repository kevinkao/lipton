<?php

namespace KevinKao\Lipton\Models;

use Illuminate\Database\Eloquent\Model;

class CmsCrawlerContentRule extends Model
{
    protected $table = 'cms_crawler_content_rule';
    protected $fillable = ['crawler_id', 'column', 'selector', 'type', 'filter'];
    public $timestamps = false;
}