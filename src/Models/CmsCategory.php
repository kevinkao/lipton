<?php

namespace KevinKao\Lipton\Models;

use Illuminate\Database\Eloquent\Model;

class CmsCategory extends Model
{
    protected $fillable = ['title', 'parent_id', 'slug', 'order', 'type', 'external_link'];

    public function posts()
    {
        return $this->hasMany(CmsPost::class);
    }

    public function subCategories()
    {
        return $this->hasMany(CmsCategory::class, 'parent_id');
    }

    public function parent()
    {
        return $this->belongsTo(CmsCategory::class, 'parent_id', 'id');
    }

    public function subPosts()
    {
        return $this->hasManyThrough(CmsPost::class, self::class, 'parent_id', 'cms_category_id');
    }

    public function scopeVisible($query)
    {
        return $query->where('visible', 1);
    }
}