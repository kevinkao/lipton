<?php

namespace KevinKao\Lipton\Models;

use Cache;

use Illuminate\Database\Eloquent\Model;

use \Carbon\Carbon;

use KevinKao\Lipton\Constant;
use KevinKao\Lipton\Scopes\VisibleCategoriesScope;
use KevinKao\Lipton\Scopes\OrderByTimeDescScope;


class CmsPost extends Model
{
    protected $table = 'cms_posts';
    protected $fillable = ['cms_category_id', 'author_id', 'title', 'slug', 'content', 'order', 'external_link', 'score', 'cover_id'];

    public function cover()
    {
        return $this->belongsTo(CmsMedia::class);
    }

    public function category()
    {
        return $this->belongsTo(CmsCategory::class, 'cms_category_id');
    }

    public function meta()
    {
        return $this->hasMany(CmsPostMeta::class, 'cms_post_id', 'id');
    }

    protected static function boot()
    {
        parent::boot();

        // static::addGlobalScope(new VisibleCategoriesScope);
        static::addGlobalScope(new OrderByTimeDescScope);
    }

    /**
     * @param  string If cover does not exists, return this default path
     * @return [string]
     */
    public function coverPath($default = '')
    {
        if (isset($this->cover)) {
            return $this->cover->path;
        }
        if (preg_match('/<img[^>]* src=\"(\/[^\"]*)\"[^>]*>/', $this->content, $matches)) {
            return $matches[1];
        }
        return $default;
    }

    /**
     * Detected cover exists in content
     * @return boolean
     */
    public function coverInPostBody()
    {
        if (empty($this->cover)) {
            return false;
        }
        return strstr($this->content, $this->cover->path) ? true : false;
    }

    /**
     * Get the full external url
     * @return string
     */
    public function getFullExternalUrl()
    {
        if (empty($this->external_link)) {
            return '';
        }
        if (preg_match('/^\//', $this->external_link)) {
            // 相對路徑
            return $this->external_link;
        }
        if (preg_match('/^(?:http)+s*:\/\/.+$/', $this->external_link)) {
            return $this->external_link;
        }
        return "http://{$this->external_link}";
    }

    public function pickRandomExternalUrl()
    {
        $urls = array_unique(explode(',', preg_replace('/[\s]+/', ',', $this->external_link)));
        return count($urls) > 0 ? $urls[array_rand($urls)] : '';
    }

    /**
     * @param  integer
     * @return string
     */
    public function retrieveTitle($len = 20)
    {
        $title = strip_tags($this->title);
        $title = str_replace(' ', '', $title);
        $title = preg_replace('/[\s\t]+/', '', $title);

        if (mb_strlen($title) > $len) {
            return mb_substr($title, 0, $len - 1) . '...';
        }
        return $title;
    }

    /**
     * Deprecated
     * @param  integer
     * @return string
     */
    public function retrieveContent($len = 40)
    {
        $content = strip_tags($this->content);
        $content = str_replace(' ', '', $content);
        $content = preg_replace('/[\s\t]+/', '', $content);

        if (mb_strlen($content) > $len) {
            return mb_substr($content, 0, $len - 1) . '...';
        }
        return $content;
    }

    /**
     * Get the content excerpt
     * @param  integer
     * @param  integer
     * @param  string
     * @return string
     */
    public function excerpt($start = 0, $width = 240, $trim_marker = '...')
    {
        return preg_replace('/\s+/', '', mb_strimwidth(trim(strip_tags($this->content)), $start, $width, '...'));
    }

    public function nextPost()
    {
        return CmsPost::where('id', '>', $this->id)->orderBy('id', 'asc')->first();
    }

    public function prevPost()
    {
        return CmsPost::where('id', '<', $this->id)->orderBy('id', 'desc')->first();
    }

    public function randomSiblings($limit = 15)
    {
        return CmsPost::where('cms_category_id', $this->cms_category_id)
                ->whereNotIn('id', [$this->id])
                ->inRandomOrder()
                ->limit($limit)
                ->get();
    }

    public function scopeByRange($query, $offset, $limit = 15)
    {
        return $query->offset($offset)->limit($limit);
    }

    public function scopeRandom($query, $limit = 15)
    {
        return $query->inRandomOrder()->limit($limit);
    }

    public function scopeRandomToday($query, $limit = 15)
    {
        $seconds = Carbon::tomorrow()->diffInSeconds(Carbon::now()) + 1;
        $key = "posts:random:today:{$limit}";
        $id = Cache::remember($key, $seconds, function() use ($limit) {
            return CmsPost::select(['id'])->inRandomOrder()->limit($limit)->get()->pluck('id');
        });
        return $query->whereIn('id', $id);
    }

    public function scopeVisibleCategories($query)
    {
        $categories = CmsCategory::select(['id'])->visible()->orderBy('order')->get();
        return $query->whereIn('cms_category_id', $categories->pluck('id'));
    }
}