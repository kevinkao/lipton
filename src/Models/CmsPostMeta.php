<?php

namespace KevinKao\Lipton\Models;

use Illuminate\Database\Eloquent\Model;

class CmsPostMeta extends Model
{
    protected $table = 'cms_post_meta';
    protected $fillable = ['cms_post_id', 'k', 'v'];
    public $timestamps = false;
}