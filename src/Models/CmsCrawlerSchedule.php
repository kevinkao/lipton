<?php

namespace KevinKao\Lipton\Models;

use Illuminate\Database\Eloquent\Model;

class CmsCrawlerSchedule extends Model
{
    protected $table = 'cms_crawler_schedule';
    public $timestamps = false;
    public $fillable = ['crawer_id', 'method', 'method_param'];
}