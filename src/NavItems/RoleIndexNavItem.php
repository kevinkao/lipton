<?php

namespace KevinKao\Lipton\NavItems;

use Gate;

/**
* 角色查詢
*/
class RoleIndexNavItem extends NavItem
{
    public $title = 'lipton::role.nav.index';
    public $iconClass = 'fa-user-tag';
    public $hasTreeView = false;

    public function getLink($fullUrl = true)
    {
        return route('lipton.role.index', [], $fullUrl);
    }

    public function isVisible()
    {
        return Gate::allows('browse', 'role');
    }
}