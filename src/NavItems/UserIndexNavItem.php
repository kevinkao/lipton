<?php

namespace KevinKao\Lipton\NavItems;

use Gate;

/**
* 用戶查詢
*/
class UserIndexNavItem extends NavItem
{
    public $title = 'lipton::user.nav.index';
    public $iconClass = 'fa-users';
    public $hasTreeView = false;

    public function getLink($fullUrl = true)
    {
        return route('lipton.user.index', [], $fullUrl);
    }

    public function isVisible()
    {
        return Gate::allows('browse', 'user');
    }
}