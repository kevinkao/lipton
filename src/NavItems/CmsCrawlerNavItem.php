<?php

namespace KevinKao\Lipton\NavItems;

use Gate;

class CmsCrawlerNavItem extends NavItem
{
    public $title = 'lipton::cms.nav.crawler.index';
    public $iconClass = 'fa-spider';
    public $hasTreeView = false;

    public function getLink($fullUrl = true)
    {
        return route('lipton.cms.crawler.index', [], $fullUrl);
    }

    public function isVisible()
    {
        return Gate::allows('browse', 'cms_crawler');
    }
}