<?php

namespace KevinKao\Lipton\NavItems;

class UserNavItem extends NavItem
{
    public $title = 'lipton::user.nav.management';
    public $iconClass = 'fa-users-cog';
    public $hasTreeView = true;
    protected $childItemsClass = [
        \KevinKao\Lipton\NavItems\UserIndexNavItem::class,
        \KevinKao\Lipton\NavItems\RoleIndexNavItem::class,
    ];

    public function getLink($fullUrl = true)
    {
        return route('lipton.user.index', [], $fullUrl) . '*';
    }

    public function isVisible()
    {
        if ($this->countVisibleChildItems() > 0) {
            return true;
        }
        return false;
    }
}