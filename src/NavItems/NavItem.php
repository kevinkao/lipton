<?php

namespace KevinKao\Lipton\NavItems;

use Request;
use Log;

abstract class NavItem
{
    abstract function getLink($fullUrl = true);
    abstract function isVisible();

    protected $childItemsClass = [];
    protected $childItemsObject = [];

    public function __construct()
    {
        foreach ($this->childItemsClass as $class) {
            $this->childItemsObject[] = resolve($class);
        }
    }

    public function getChildItems()
    {
        return $this->childItemsObject;
    }

    public function countVisibleChildItems()
    {
        $count = 0;
        foreach($this->childItemsObject as $child) {
            if ($child->isVisible()) {
                $count++;
            }
        }
        return $count;
    }

    public function isActivited()
    {
        $anyActivited = false;
        if ($this->hasTreeView) {
            foreach($this->childItemsObject as $child) {
                $path = $child->getLink(false);
                if (Request::is(substr($path, 1))) {
                    $anyActivited = true;
                    break;
                }
            }
        } else {
            $path = $this->getLink(false);
            $anyActivited = Request::is(substr($path, 1).'*');
        }

        return $anyActivited;
    }
}