<?php

namespace KevinKao\Lipton\NavItems;

use Gate;

class CmsCategoryNavItem extends NavItem
{
    public $title = 'lipton::cms.nav.category.index';
    public $iconClass = 'fa-list';
    public $hasTreeView = false;

    public function getLink($fullUrl = true)
    {
        return route('lipton.cms.category.index', [], $fullUrl);
    }

    public function isVisible()
    {
        return Gate::allows('browse', 'cms_category');
    }
}