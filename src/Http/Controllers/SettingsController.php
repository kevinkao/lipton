<?php

namespace KevinKao\Lipton\Http\Controllers;

use Illuminate\Http\Request;
use KevinKao\Lipton\Models\Settings;
use \KevinKao\Lipton\Models\CmsMedia;
use \KevinKao\Lipton\Constants;
use Log;
use Storage;

class SettingsController extends Controller
{
    public function index()
    {
        $this->authorize('browse', 'settings');

        $settings = Settings::orderBy('order')->get();
        return view('lipton::settings.index', compact('settings'));
    }

    public function update(Request $request)
    {
        $this->authorize('edit', 'settings');

        try {
            $section = Settings::where('key', $request->input('key'))->firstOrFail();
            $section->value = trim($request->input('value'));
            $section->save();
            return response()->json([
                'code' => 0,
                'data' => $section
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'code' => $e->getCode() > 0 ? $e->getCode() : 99,
                'message' => $e->getMessage()
            ]);
        }
    }

    public function upload(Request $request)
    {
        try {
            $section = Settings::where('key', $request->input('key'))->firstOrFail();

            if ($request->hasFile('image')) {
                $path = Storage::url($request->file('image')->store(config('lipton.media_path'), ['disk' => 'public']));
                $media = new CmsMedia();
                $media->path = $path;
                $media->type = Constants::CMS_MEDIA_TYPE_IMAGE;
                $media->save();

                $section->value = $path;
                $section->save();
            }
            return response()->json([
                'code' => 0,
                'data' => $section
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'code' => $e->getCode() > 0 ? $e->getCode() : 99,
                'message' => $e->getMessage()
            ]);
        }
    }
}
