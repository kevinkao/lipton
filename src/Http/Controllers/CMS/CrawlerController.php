<?php

namespace KevinKao\Lipton\Http\Controllers\CMS;

use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use KevinKao\Lipton\Http\Controllers\Controller;
use KevinKao\Lipton\Errors;
use KevinKao\Lipton\Models\CmsCategory;
use KevinKao\Lipton\Constants;
use KevinKao\Lipton\Models\CmsCrawler;
use KevinKao\Lipton\Models\CmsCrawlerSchedule;
use KevinKao\Lipton\Models\CmsCrawlerContentRule;
use KevinKao\Lipton\CMS\Frequency;
use KevinKao\Lipton\CMS\CrawlerCommand;
use KevinKao\Lipton\CMS\UserAgent;
use Log;
use DB;

use QL\QueryList;

class CrawlerController extends Controller
{
    protected $frequency;

    public function __construct()
    {
        $this->frequency = [
            Frequency::EVERY_MINUTE         => __('lipton::cms.crawler.frequency.'.Frequency::EVERY_MINUTE),
            Frequency::EVERY_FIVE_MINUTE    => __('lipton::cms.crawler.frequency.'.Frequency::EVERY_FIVE_MINUTE),
            Frequency::EVERY_TEN_MINUTE     => __('lipton::cms.crawler.frequency.'.Frequency::EVERY_TEN_MINUTE),
            Frequency::EVERY_FIFTEEN_MINUTE => __('lipton::cms.crawler.frequency.'.Frequency::EVERY_FIFTEEN_MINUTE),
            Frequency::EVERY_THIRTY_MINUTE  => __('lipton::cms.crawler.frequency.'.Frequency::EVERY_THIRTY_MINUTE),
            Frequency::HOURLY               => __('lipton::cms.crawler.frequency.'.Frequency::HOURLY),
            // Frequency::EVERY_TWO_HOURS      => __('lipton::cms.crawler.frequency.'.Frequency::EVERY_TWO_HOURS),
            // Frequency::EVERY_THREE_HOURS    => __('lipton::cms.crawler.frequency.'.Frequency::EVERY_THREE_HOURS),
            // Frequency::EVERY_FOUR_HOURS     => __('lipton::cms.crawler.frequency.'.Frequency::EVERY_FOUR_HOURS),
            Frequency::DAILY                => __('lipton::cms.crawler.frequency.'.Frequency::DAILY),
            Frequency::WEEKLY               => __('lipton::cms.crawler.frequency.'.Frequency::WEEKLY),
            Frequency::MONTHLY              => __('lipton::cms.crawler.frequency.'.Frequency::MONTHLY),
        ];
    }

    public function index()
    {
        $categories = CmsCategory::all();
        $frequency = $this->frequency;
        return view('lipton::cms.crawler.index', compact('categories', 'frequency'));
    }

    public function store(Request $request)
    {
        try {
            $this->authorize('create', 'cms_crawler');

            $this->validate($request, [
                'title' => 'required|regex:/^[\w\p{Han}]{1,25}$/u',
                'cms_category_id' => 'nullable|numeric'
            ], [
                'title.required' => __('lipton::cms.crawler.feedback.is_required', ['field' => __('lipton::cms.crawler.modal.title')]),
                'title.regex'    => __('lipton::cms.crawler.feedback.not_match', ['field' => __('lipton::cms.crawler.modal.title')]),
                'cms_category_id.numeric'  => __('lipton::cms.crawler.feedback.not_match', ['field' => __('lipton::cms.crawler.modal.cms_category_id')]),
            ]);

            DB::beginTransaction();
            $crawler = CmsCrawler::create($request->all());
            $crawler->schedule()->create(['crawler_id' => $crawler->id]);
            $crawler->listRule()->create(['crawler_id' => $crawler->id]);
            DB::commit();
            return response()->json([
                'code' => 0,
                'message' => __('lipton::cms.crawler.feedback.create_success'),
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e->getMessage());
            return response()->json([
                'code' => $e->getCode() == 0 ? Errors::UNKNOWN_ERROR : intval($e->getCode()),
                'message' => $e->getMessage()
            ]);
        }
    }

    public function datatable(Request $request)
    {
        $this->authorize('browse', 'cms_crawler');

        $columns = ['id', 'title', 'cms_category_id'];
        $isSuperAdmin = auth()->user()->isSuperAdmin();

        $totalRows = CmsCrawler::count();
        $totalFiltered = $totalRows;

        $limit = $request->input('length');
        $start = $request->input('start');

        $data = CmsCrawler::offset($start)->limit($limit)->get();

        $userCanEdit = auth()->user()->can('edit', 'cms_crawler');
        $userCanDelete = auth()->user()->can('delete', 'cms_crawler');
        foreach ($data as &$crawler) {
            $crawler->canEdit = $userCanEdit;
            $crawler->canDelete = $userCanDelete;
            $crawler->category_title = isset($crawler->category) ? $crawler->category->title : '-';
            $crawler->frequency = $this->frequency[$crawler->schedule->method];
            $crawler->domain = $crawler->domain;
            $editAction = '<button data-id="'.$crawler->id.'" data-toggle="modal" data-target="#cms-crawler-modal" class="btn bg-gradient-secondary btn-sm">' .
                '<i class="fa fa-edit mr-0" style="width: 11px"></i></button>';
            $deleteAction = '<a href="javascript:void(0)" data-id="'.$crawler->id.'" class="btn bg-gradient-danger btn-sm action delete ml-1">' .
                '<i class="fa fa-trash mr-0"></i></a>';
            $crawler->action = ($userCanEdit ? $editAction : '') . ($userCanDelete ? $deleteAction : '');
        }

        return response()->json([
            'draw'            => intval($request->input('draw')),
            'recordsTotal'    => intval($totalRows),
            "recordsFiltered" => intval($totalFiltered),
            'data' => $data
        ]);
    }

    public function update(Request $request, $id)
    {
        try {
            $this->authorize('edit', 'cms_crawler');

            $this->validate($request, [
                'title' => 'required|regex:/^[\w\p{Han}]{1,25}$/u',
                'cms_category_id' => 'nullable|numeric'
            ], [
                'title.required' => __('lipton::cms.crawler.feedback.is_required', ['field' => __('lipton::cms.crawler.modal.title')]),
                'title.regex'    => __('lipton::cms.crawler.feedback.not_match', ['field' => __('lipton::cms.crawler.modal.title')]),
                'cms_category_id.numeric'  => __('lipton::cms.crawler.feedback.not_match', ['field' => __('lipton::cms.crawler.modal.cms_category_id')]),
            ]);

            $crawler = CmsCrawler::findOrFail($id);
            $crawler->fill($request->all());
            $crawler->save();

            return response()->json([
                'code' => 0,
                'message' => __('lipton::cms.crawler.feedback.update_success'),
            ]);
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return response()->json([
                'code' => $e->getCode() == 0 ? Errors::UNKNOWN_ERROR : intval($e->getCode()),
                'message' => $e->getMessage()
            ]);
        }
    }

    public function operation(Request $request, $id)
    {
        try {
            $this->authorize('edit', 'cms_crawler');

            $category = CmsCrawler::findOrFail($id);
            $category->operation = $request->input('operation');
            $category->save();
            return response()->json([
                'code' => 0,
                'message' => __("lipton::cms.crawler.feedback.update_operation_success", [
                    'title' => $category->title,
                    'status' => $category->operation == 0 ?
                        __("lipton::cms.crawler.feedback.operation_stop") :
                        __("lipton::cms.crawler.feedback.operation_start")
                ]),
            ]);
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return response()->json([
                'code' => $e->getCode() == 0 ? Errors::UNKNOWN_ERROR : intval($e->getCode()),
                'message' => $e->getMessage()
            ]);
        }
    }

    public function destroy($id)
    {
        try {
            $this->authorize('delete', 'cms_crawler');

            $crawler = CmsCrawler::findOrFail($id);
            $crawler->delete();

            return response()->json([
                'code' => 0,
                'message' => __("lipton::cms.crawler.feedback.destroy_success"),
            ]);
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return response()->json([
                'code' => $e->getCode() == 0 ? Errors::UNKNOWN_ERROR : intval($e->getCode()),
                'message' => $e->getMessage()
            ]);
        }
    }

    public function getSchedule(Request $request, $id)
    {
        try {
            $this->authorize('browse', 'cms_crawler');
            $crawler = CmsCrawler::findOrFail($id);
            return response()->json([
                'code' => 0,
                'data' => $crawler->fresh()->schedule
            ]);
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return response()->json([
                'code' => $e->getCode() == 0 ? Errors::UNKNOWN_ERROR : intval($e->getCode()),
                'message' => $e->getMessage()
            ]);
        }
    }

    public function updateSchedule(Request $request, $id)
    {
        try {
            $this->authorize('edit', 'cms_crawler');
            $crawler = CmsCrawler::findOrFail($id);

            $crawler->schedule->fill($request->all());
            $crawler->schedule->save();

            return response()->json([
                'code' => 0,
                'message' => __("lipton::cms.crawler.feedback.update_schedule_success"),
            ]);
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return response()->json([
                'code' => $e->getCode() == 0 ? Errors::UNKNOWN_ERROR : intval($e->getCode()),
                'message' => $e->getMessage()
            ]);
        }
    }

    public function updateRules(Request $request, $id)
    {
        try {
            $this->authorize('edit', 'cms_crawler');
            $crawler = CmsCrawler::findOrFail($id);

            $request->validate([
                'list.limit' => 'nullable|numeric',
                'list.start_number' => 'numeric',
                'list.end_number' => 'numeric',
            ]);

            DB::beginTransaction();
            $crawler->listRule()->update([
                'start_url' => $request->input('list.start_url'),
                'limit'     => $request->input('list.limit'),
                'range'     => $request->input('list.range'),
                'selector'  => $request->input('list.selector'),
                'start_number' => $request->input('list.start_number'),
                'end_number' => $request->input('list.end_number'),
                'followup_action' => $request->input('list.followup_action')
            ]);

            $crawler->contentRule()->delete();
            $rules = [];
            foreach($request->input('content') as $rule) {
                $rule['crawler_id'] = $crawler->id;
                $rules[] = new CmsCrawlerContentRule($rule);
            }
            $crawler->contentRule()->saveMany($rules);

            DB::commit();
            return response()->json([
                'code' => 0,
                'message' => __("lipton::cms.crawler.feedback.update_rules_success"),
            ]);
        } catch (ValidationException $e) {
            return response()->json([
                'code' => $e->getCode() == 0 ? Errors::UNKNOWN_ERROR : intval($e->getCode()),
                'message' => $e->getMessage(),
                'erros' => $e->errors()
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e->getMessage());
            return response()->json([
                'code' => $e->getCode() == 0 ? Errors::UNKNOWN_ERROR : intval($e->getCode()),
                'message' => $e->getMessage()
            ]);
        }
    }

    public function rules(Request $request, $id)
    {
        try {
            $this->authorize('browse', 'cms_crawler');
            $crawler = CmsCrawler::findOrFail($id);
            return response()->json([
                'code' => 0,
                'data' => [
                    'list' => $crawler->listRule,
                    'content' => $crawler->contentRule
                ]
            ]);
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return response()->json([
                'code' => $e->getCode() == 0 ? Errors::UNKNOWN_ERROR : intval($e->getCode()),
                'message' => $e->getMessage()
            ]);
        }
    }

    public function testListUrl(Request $request)
    {
        try {
            $request->validate([
                'limit' => 'nullable|numeric'
            ]);
            $startUrl = trim(str_replace('%number%', 1, $request->input('start_url')));
            $startUrls = explode(PHP_EOL, $startUrl);

            $result = [];
            foreach($startUrls as $url) {
                $ql = QueryList::get($url, [], [
                            'headers' => [
                                'User-Agent' => UserAgent::random()
                            ]
                        ])
                        ->rules([
                            'link' => [$request->input('selector'), 'href']
                        ])
                        // ->removeHead()
                        ->range($request->input('range'))
                        ->query();

                $result = array_merge($result, $ql->getData()->all());
            }

            if ($request->has('limit')) {
                $data = array_slice($result, 0, $request->input('limit'));
            }

            return response()->json([
                'code' => 0,
                'data' => $data
            ]);
        } catch (ValidationException $e) {
            return response()->json([
                'code' => $e->getCode() == 0 ? Errors::UNKNOWN_ERROR : intval($e->getCode()),
                'message' => $e->getMessage(),
                'erros' => $e->errors()
            ]);
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return response()->json([
                'code' => $e->getCode() == 0 ? Errors::UNKNOWN_ERROR : intval($e->getCode()),
                'message' => $e->getMessage()
            ]);
        }
    }

    public function testContentUrl(Request $request)
    {
        try {
            $request->validate([
                'crawler_id' => 'required',
                'url' => 'required|regex:/.+/'
            ], [
                'crawler_id.required' => 'crawler id required',
                'url.required' => 'url required',
                'url.regex' => 'invaliud url'
            ]);

            $crawler = CmsCrawler::find($request->input('crawler_id'));
            if (empty($crawler)) {
                throw new \Exception(sprintf("No such crawler id: %s", $request->input('crawler_id')));
            }

            $url = trim($request->input('url'));
            $rules = [];
            foreach($request->input('rules') as $rule) {
                $rules[$rule['column']] = [$rule['selector'], $rule['type'], $rule['filter']];
            }

            $ql = QueryList::get($url, [], [
                        'headers' => [
                            'User-Agent' => UserAgent::random()
                        ]
                    ])
                    ->rules($rules);

            if ($crawler->charset === Constants::CMS_CRAWLER_CHARSET_GB2312) {
                $ql->encoding('UTF-8', 'GB2312')->removeHead();
            }

            $ql->query();
            return response()->json([
                'code' => 0,
                'data' => $ql->getData()->all()
            ]);
        } catch (ValidationException $e) {
            return response()->json([
                'code' => $e->getCode() == 0 ? Errors::UNKNOWN_ERROR : intval($e->getCode()),
                'message' => $e->getMessage(),
                'erros' => $e->errors()
            ]);
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return response()->json([
                'code' => $e->getCode() == 0 ? Errors::UNKNOWN_ERROR : intval($e->getCode()),
                'message' => $e->getMessage()
            ]);
        }
    }

    public function trigger(Request $request, $id)
    {
        try {
            $crawler = CmsCrawler::find($id);
            if (empty($crawler)) {
                throw new \Exception("No such crawler");
            }
            $crawler->command_status = CrawlerCommand::PENDING;
            $crawler->save();
            Log::channel('crawler')->info("{$crawler->title} has been triggered.");

            return response()->json([
                'code' => 0,
                'message' => 'OK'
            ]);
        } catch (\Exception $e) {
            Log::channel('crawler')->error($e->getMessage());
            return response()->json([
                'code' => $e->getCode() == 0 ? Errors::UNKNOWN_ERROR : intval($e->getCode()),
                'message' => $e->getMessage()
            ]);
        }
    }

    public function log()
    {
        try {
            //how many lines?
            $linecount=50;

            //what's a typical line length?
            $length=40;

            //which file?
            $file=storage_path('logs/crawler-'.date('Y-m-d').'.log');

            //we double the offset factor on each iteration
            //if our first guess at the file offset doesn't
            //yield $linecount lines
            $offset_factor=1;


            $bytes=filesize($file);

            $fp = fopen($file, "r");
            if (!$fp) {
                throw new \Exception();
            }

            //read all following lines, store last x
            $lines=[];
            $complete=false;
            while (!$complete) {
                //seek to a position close to end of file
                $offset = $linecount * $length * $offset_factor;
                fseek($fp, -$offset, SEEK_END);


                //we might seek mid-line, so read partial line
                //if our offset means we're reading the whole file, 
                //we don't skip...
                if ($offset<$bytes)
                    fgets($fp);

                
                
                while(!feof($fp)) {
                    $line = fgets($fp);
                    array_push($lines, $line);
                    if (count($lines)>$linecount)
                    {
                        array_shift($lines);
                        $complete=true;
                    }
                }

                //if we read the whole file, we're done, even if we
                //don't have enough lines
                if ($offset>=$bytes) {
                    $complete=true;
                }  else {
                    $offset_factor*=2; //otherwise let's seek even further back
                }
            }
            return $lines;
        } catch (\Exception $e) {
            Log::channel('crawler')->error($e->getMessage());
            return [];
        }
    }
}