<?php

namespace KevinKao\Lipton\Http\Controllers\CMS;

use Illuminate\Http\Request;
use KevinKao\Lipton\Http\Controllers\Controller;
use KevinKao\Lipton\Errors;
use KevinKao\Lipton\Models\CmsCategory;
use KevinKao\Lipton\Models\CmsPostComment;
use KevinKao\Lipton\Constants;
use Log;

class CommentController extends Controller
{
    public function __construct()
    {
        // 
    }

    public function index(Request $request)
    {
        $this->authorize('browse', 'cms_comment');

        if ($request->ajax()) {
            return response()->json([
                'code' => 0,
                'data' => CmsPostComment::all()
            ]);
        }
        return view('lipton::cms.comment.index');
    }

    public function exclude(Request $request, $id)
    {
        $this->authorize('browse', 'cms_comment');

        if ($request->ajax()) {
            return response()->json([
                'code' => 0,
                'data' => CmsCategory::whereNotIn('id', [$id])->get()
            ]);
        }
    }

    public function datatable(Request $request)
    {
        $this->authorize('browse', 'cms_comment');

        $columns = ['post_title', 'author', 'contnet_brief', 'created_at', 'status', 'actions'];
        $isSuperAdmin = auth()->user()->isSuperAdmin();
        
        $totalRows = CmsPostComment::count();
        $totalFiltered = $totalRows;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            $query = CmsPostComment::offset($start)
                            ->limit($limit)
                            ->orderBy($order, $dir);

            $rows = $query->get();
        } else {
            $search = $request->input('search.value');
            $rows = CmsPostComment::where('title', 'LIKE', "{$search}%")
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order, $dir)
                            ->get();

            $totalFiltered = $rows->count();
        }

        $data = [];
        $userCanEdit = auth()->user()->can('edit', 'cms_comment');
        $userCanDelete = auth()->user()->can('delete', 'cms_comment');
        foreach ($rows as $row) {
            $nestedData['canEdit'] = $userCanEdit;
            $nestedData['canDelete'] = $userCanDelete;
            $nestedData['id'] = $row->id;
            $nestedData['post_id'] = isset($row->post) ? $row->post->id : null;
            $nestedData['post_title'] = isset($row->post) ? $row->post->title : '';
            $nestedData['post_title_brief'] = isset($row->post) ? $row->post->retrieveTitle(14) : '';
            $nestedData['author'] = $row->author->name;
            $nestedData['author_id'] = $row->author->id;
            $nestedData['content'] = $row->content;
            $nestedData['contnet_brief'] = $row->retrieveContent(10);
            $nestedData['status'] = $row->status;
            $nestedData['created_at'] = \Carbon\Carbon::parse($row->created_at)->format("Y-m-d H:i:s");

            $nestedData['actions'] = '';
            // $editAction = '<button class="btn bg-gradient-secondary btn-sm action update" data-id="'.$row->id.'" data-toggle="modal" data-target="#cms-comment-modal"><i class="fa fa-edit mr-0" style="width: 11px"></i></button>';
            // $deleteAction = '<button class="btn bg-gradient-danger btn-sm action delete" data-id="'.$row->id.'"><i class="fas fa-trash"></i></button>';
            // $nestedData['actions'] = $userCanEdit ? $editAction : '';
            // $nestedData['actions'] .= $userCanDelete ? $deleteAction : '';
            $data[] = $nestedData;
        }

        return response()->json([
            'draw'            => intval($request->input('draw')),
            'recordsTotal'    => intval($totalRows),
            "recordsFiltered" => intval($totalFiltered),
            'data' => $data
        ]);
    }

    public function checkToggle(Request $request, $id)
    {
        try {
            $this->authorize('edit', 'cms_comment');

            $comment = CmsPostComment::findOrFail($id);
            $comment->status = $request->input('status');
            $comment->save();
            return response()->json([
                'code' => 0,
                'message' => __("lipton::common.actions.success"),
            ]);
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return response()->json([
                'code' => $e->getCode() == 0 ? Errors::UNKNOWN_ERROR : intval($e->getCode()),
                'message' => __("lipton::common.actions.fail", ['message' => $e->getMessage()])
            ]);
        }
    }

    public function destroy(Request $request, $id)
    {
        try {
            $this->authorize('delete', 'cms_comment');

            $comment = CmsPostComment::findOrFail($id);
            $comment->delete();
            return response()->json([
                'code' => 0,
                'message' => __("lipton::common.actions.success"),
            ]);
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return response()->json([
                'code' => $e->getCode() == 0 ? Errors::UNKNOWN_ERROR : intval($e->getCode()),
                'message' => __("lipton::common.actions.fail", ['message' => $e->getMessage()])
            ]);
        }
    }
}