<?php

namespace KevinKao\Lipton\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

use KevinKao\Lipton\Models\Role;
use KevinKao\Lipton\Models\User;
use KevinKao\Lipton\Errors;
use Log;
use DB;

class UserController extends Controller
{
    public function index()
    {
        $this->authorize('browse', 'user');

        $roles = Role::exceptSuperAdmin()->get();
        return view('lipton::user.index', compact('roles'));
    }

    public function showAuth()
    {
        $user = auth()->user()->fresh();
        return response()->json([
            'code' => 0,
            'data' => $user
        ]);
    }

    public function datatable(Request $request)
    {
        $this->authorize('browse', 'user');

        $columns = ['id', 'name', 'account', 'email', 'phone_number', 'qq', 'created_at'];
        
        $totalRows = User::count();
        $totalFiltered = $totalRows;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            $users = User::offset($start)
                            ->limit($limit)
                            ->orderBy($order, $dir)
                            ->get();
        } else {
            $search = $request->input('search.value');

            $users = User::where('name', 'LIKE', "%{$search}%")
                            ->orWhere('email', 'LIKE', "%{$search}%")
                            ->with('roles')
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order, $dir)
                            ->get();

            $totalFiltered = User::where('name', 'LIKE', "%{$search}%")
                                    ->orWhere('email', 'LIKE', "%{$search}%")
                                    ->count();
        }

        $data = [];
        $userCanEdit = auth()->user()->can('edit', 'user');
        $userCanDelete = auth()->user()->can('delete', 'user');
        foreach ($users as $user) {
            $nestedData['id'] = $user->id;
            $nestedData['name'] = $user->name;
            $nestedData['account'] = $user->account;
            $nestedData['email'] = $user->email;
            $nestedData['phone_number'] = $user->phone_number;
            $nestedData['qq'] = $user->qq;
            $nestedData['roles'] = $user->roles;
            $editAction = '<button class="btn bg-gradient-secondary btn-sm action update" data-id="'.$user->id.'" data-toggle="modal" data-target="#user-update-modal"><i class="fa fa-edit mr-0" style="width: 11px"></i></button>';
            // $blockAction = '<button class="btn bg-gradient-warning btn-sm action block" data-id="'.$user->id.'"><i class="fas fa-lock"></i></button>';
            $deleteAction = '<button class="btn bg-gradient-danger btn-sm action delete" data-id="'.$user->id.'"><i class="fas fa-trash"></i></button>';

            $nestedData['actions'] = $userCanEdit ? $editAction : '';
            $nestedData['actions'] .= $userCanDelete ? $deleteAction : '';

            $data[] = $nestedData;
        }
        return response()->json([
            'draw'            => intval($request->input('draw')),
            'recordsTotal'    => intval($totalRows),
            "recordsFiltered" => intval($totalFiltered),
            'data'            => $data
        ]);
    }

    public function store(Request $request)
    {
        $this->authorize('create', 'user');

        try {
            $this->validate($request, [
                'name'             => 'required',
                'email'            => 'required|email',
                'password'         => 'required|confirmed',
                'password_confirmation' => 'required',
            ], [
                'name.required'                  => __('lipton::user.message.field_required', ['field' => __('lipton::user.modal.name')]),
                'email.required'                 => __('lipton::user.message.field_required', ['field' => __('lipton::user.modal.email')]),
                'password.required'              => __('lipton::user.message.field_required', ['field' => __('lipton::user.modal.password')]),
                'password.confirmed'             => __('lipton::user.message.password_different'),
                'password_confirmation.required' => __('lipton::user.message.field_required', ['field' => __('lipton::user.modal.password_confirmation')]),
            ]);

            DB::beginTransaction();
            $user = new User();
            $user->name = $request->input('name');
            $user->email = $request->input('email');
            $user->phone_number = $request->input('phone_number', null);
            $user->qq = $request->input('qq', null);
            $user->password = bcrypt($request->input('password'));
            $user->save();
            $user->roles()->attach($request->input('roles'));
            DB::commit();
            return response()->json([
                'code' => 0,
                'data' => $user
            ]);
        } catch(ValidationException $e) {
            return response()->json([
                'code' => Errors::VALIDATION_FAILED,
                'message' => $e->errors()
            ]);
        } catch(\Exception $e) {
            DB::rollBack();
            Log::error($e->getMessage());
            return response()->json([
                'code' => $e->getCode() == 0 ? Errors::UNKNOWN_ERROR : intval($e->getCode()),
                'message' => $e->getMessage()
            ]);
        }
    }

    public function update(Request $request, $id)
    {
        $this->authorize('edit', 'user');

        try {
            $this->validate($request, [
                'name'     => 'required',
                'password' => 'confirmed',
            ], [
                'name.required'         => __('lipton::user.message.field_required', ['field' => __('lipton::user.modal.name')]),
                'password.confirmed'    => __('lipton::message.form_field_password_different'),
                'password_confirmation' => __('lipton::user.message.password_different'),
            ]);

            DB::beginTransaction();
            $user = User::findOrFail($id);
            // 確認是否有編輯該user的權限
            $this->authorize('edit', $user);
            if ($request->input('password') !== null && $request->input('password_confirmation') !== null) {
                $user->password = bcrypt($request->input('password'));
            }
            $user->fill($request->all());
            $user->save();
            if ($request->exists('roles')) {
                $user->roles()->sync($request->input('roles'));
            }

            DB::commit();
            return response()->json([
                'code' => 0,
                'data' => $user
            ]);
        } catch(ValidationException $e) {
            return response()->json([
                'code' => Errors::VALIDATION_FAILED,
                'message' => $e->errors()
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e->getMessage());
            return response()->json([
                'code' => $e->getCode() == 0 ? Errors::UNKNOWN_ERROR : intval($e->getCode()),
                'message' => $e->getMessage()
            ]);
        }
    }

    public function destroy(Request $request, $id)
    {
        $this->authorize('delete', 'user');

        try {
            $user = User::findOrFail($id);
            // 確認是否有刪除該user的權限
            $this->authorize('delete', $user);
            $user->delete();
            return response()->json(['code' => 0]);
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return response()->json([
                'code' => $e->getCode() == 0 ? Errors::UNKNOWN_ERROR : intval($e->getCode()),
                'message' => $e->getMessage()
            ]);
        }
    }
}
