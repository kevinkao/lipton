<?php

namespace KevinKao\Lipton\Http\Controllers;

use Illuminate\Http\Request;
use App;

class IndexController extends Controller
{
    public function index()
    {
        return redirect()->route(config('lipton.default_route'));
    }
}
