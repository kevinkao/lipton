<?php

namespace KevinKao\Lipton\Scopes;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

use KevinKao\Lipton\Models\CmsCategory;

class VisibleCategoriesScope implements Scope
{
    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {
        $categories = CmsCategory::select(['id'])->visible()->orderBy('order')->get();
        $builder->whereIn('cms_category_id', $categories->pluck('id'));
    }
}