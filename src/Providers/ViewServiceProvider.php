<?php

namespace KevinKao\Lipton\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;


class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::directive('datatable_locale', function() {
            return '<script>$.extend( true, $.fn.DataTable.defaults, {
                "language": {
                    "info": "'. __('lipton::datatable.info').'",
                    "infoEmpty": "'. __('lipton::datatable.infoEmpty') .'",
                    "infoFiltered": "'. __('lipton::datatable.infoFiltered') .'",
                    "lengthMenu": "'. __('lipton::datatable.lengthMenu') .'",
                    "search": "'. __('lipton::datatable.search') .'",
                    "zeroRecords": "'. __('lipton::datatable.zeroRecords') .'",
                    "paginate": {
                        "first": "'. __('lipton::datatable.paginate.first') .'",
                        "last": "'. __('lipton::datatable.paginate.last') .'",
                        "next": "'. __('lipton::datatable.paginate.next') .'",
                        "previous": "'. __('lipton::datatable.paginate.previous') .'"
                    }
                }
            });</script>';
        });

        View::composer(
            ['lipton::sidebar'],
            'KevinKao\Lipton\Http\View\Composers\SidebarComposer'
        );
    }
}