<?php

namespace KevinKao\Lipton\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use DB;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        \KevinKao\Lipton\Models\User::class => \KevinKao\Lipton\Policies\UserPolicy::class
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate  $gate
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::before(function ($user, $ability) {
            if ($user->isSuperAdmin()) {
                return true;
            }
        });

        Gate::define('create', $this->_handler('create'));
        Gate::define('delete', $this->_handler('delete'));
        Gate::define('edit', $this->_handler('edit'));
        Gate::define('browse', $this->_handler('browse'));
    }

    private function _handler ($permission)
    {
        return function ($user, $slug) use ($permission) {
            if (is_string($slug)) {
                // 針對系統功能去做角色權限判斷
                $ownPermissions = DB::table('users')
                    ->select('permissions.*')
                    ->leftJoin('user_roles', 'users.id', '=', 'user_roles.user_id')
                    ->leftJoin('roles', 'user_roles.role_id', '=', 'roles.id')
                    ->leftJoin('permission_role', 'roles.id', '=', 'permission_role.role_id')
                    ->leftJoin('permissions', 'permission_role.permission_id', '=', 'permissions.id')
                    ->where('users.id', $user->id)
                    ->get();

                $permission = "{$slug}_{$permission}";
                return $ownPermissions->contains('key', $permission);
            }
            // 如果為其他非功能字串slug, 這邊不多做判斷
            // 交給個別Policy自行去做判斷
            return true;
        };
    }
}