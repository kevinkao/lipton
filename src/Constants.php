<?php

namespace KevinKao\Lipton;

class Constants
{
    const ORDER_ASC                       = 'asc';
    const ORDER_DESC                      = 'desc';
    
    const CMS_CATEGORY_TYPE_GENERAL       = 1;
    const CMS_CATEGORY_TYPE_EXTERNAL_LINK = 2;
    
    const CMS_CATEGORY_VISIBLE_YES        = 1;
    const CMS_CATEGORY_VISIBLE_NO         = 0;
    
    const CMS_MEDIA_TYPE_IMAGE            = 0;
    
    const CMS_CRAWLER_FOLLOWACTION_1      = 1; // 只採集結束號碼
    const CMS_CRAWLER_FOLLOWACTION_2      = 2; // 終止採集
    
    const CMS_CRAWLER_CHARSET_UTF8        = 'utf8';
    const CMS_CRAWLER_CHARSET_GB2312      = 'gb2312';
}