<?php

namespace KevinKao\Lipton\Facades;

use Illuminate\Support\Facades\Facade;

class LiptonFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'lipton';
    }
}