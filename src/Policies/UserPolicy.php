<?php

namespace KevinKao\Lipton\Policies;

use KevinKao\Lipton\Models\User;

class UserPolicy
{
    public function edit ($currentUser, $user)
    {
        if ($user->isSuperAdmin()) {
            // Super admin can NOT be edit!!
            return false;
        }
        return true;
    }

    public function delete ($currentUser, $user)
    {
        if ($user->isSuperAdmin()) {
            // Super admin can NOT be delete!!
            return false;
        }
        return true;
    }
}