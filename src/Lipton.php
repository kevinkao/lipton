<?php

namespace KevinKao\Lipton;

use KevinKao\Lipton\Models\Settings;
use Route;

class Lipton
{
    public $settings = [];

    public function route($closure)
    {
        Route::group([
            'prefix' => config('lipton.admin_prefix'),
            'middleware' => ['web', 'admin.user']
        ], function() use ($closure) {
            $closure();
        });
    }

    public function assets($path, $secure = null)
    {
        return asset(config('lipton.assets_path') . '/' . $path, $secure);
    }

    public function settings($key, $default = null)
    {
        if (empty($this->settings)) {
            $settings = Settings::get();
            foreach($settings as $section) {
                $this->settings[$section->key] = $section->value;
            }
        }
        if (!isset($this->settings[$key]) || is_null($this->settings[$key])) {
            return $default;
        }
        return $this->settings[$key];
    }
}