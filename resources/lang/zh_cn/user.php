<?php

return [
    'nav' => [
        'management' => '用户管理',
        'create' => '添加用户',
        'index' => '用户查询',
    ],
    'toolbar' => [
        'create' => '添加',
        'fresh' => '刷新'
    ],
    'toast' => [
        'error' => '错误'
    ],
    'modal' => [
        'title_create' => '添加用户',
        'title_update' => '编辑用户',
        'close' => '关闭',
        'submit' => '提交',
        'name' => '名称',
        'email' => 'email',
        'password' => '密码',
        'password_confirmation' => '密码确认',
        'phone_number' => '手机号码',
        'qq' => 'QQ',
        'role' => '角色',
        'chose_roles' => '请选择角色',
        'password_tip' => '不更改密码留空即可'
    ],
    'feedback' => [
        'invalid_name' => '请填入正确名称',
        'invalid_email' => '请填入正确email格式',
        'invalid_password' => '密码最少短长度为8个字元',
        'invalid_password_confirmed' => '密码确认与密码不相同',
    ],
    'table' => [
        'id' => '编号',
        'name' => '名称',
        'account' => '帐号',
        'email' => '邮箱',
        'phone_number' => '手机号码',
        'created_at' => '添加时间',
        'action' => '操作',
    ],
    'message' => [
        'field_required' => ':field 为必填栏位',
        'password_different' => '密码确认与密码不相同',
        'access_denied' => '拒绝存取',
        'create_failed' => '新增失败, 请联系维护人员',
        'create_invalid' => '添加失败, 请确认填写内容',
        'create_success' => '添加成功',
        'update_failed' => '更新失败, 请联系维护人员',
        'update_invalid' => '更新失败, 请确认填写内容',
        'update_success' => '更新成功',
        'delete_confirmed' => '确认删除该用户吗?',
        'delete_tip' => '删除后就不能复原!',
        'delete_failed' => '删除失败, 请联系维护人员',
        'delete_success' => '删除成功',
        'confirm_text' => '是的',
        'cancel_text' => '取消'
    ]
];