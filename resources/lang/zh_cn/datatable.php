<?php

return [
    "decimal"        => "",
    "emptyTable"     => "No data available in table",
    "info"           =>  "显示 _START_ 到 _END_ 共 _TOTAL_ 笔",
    "infoEmpty"      => "显示 第 0 笔 到 第 0 笔 共 0 笔",
    "infoFiltered"   => "- 从 __MAX__ 项结果中过滤",
    "infoPostFix"    => "",
    "thousands"      => ",",
    "lengthMenu"     => "显示 _MENU_ 笔",
    "loadingRecords" => "载入中...",
    "processing"     => "处理中...",
    "search"         => "搜寻:",
    "zeroRecords"    => "没有符合资料",
    "paginate" => [
        "first"    => "最前页",
        "last"     => "最后页",
        "next"     => "下页",
        "previous" => "上页"
    ],
    "aria" => [
        "sortAscending"  => ": activate to sort column ascending",
        "sortDescending" => ": activate to sort column descending"
    ]
];