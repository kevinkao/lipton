<?php

return [
    'display' => [
        'super_admin' => '超级管理员',
        'admin' => '管理员',
        'general' => '一般用户',
    ],
    'nav' => [
        'index' => '角色管理'
    ],
    'toolbar' => [
        'create' => '添加',
        'fresh' => '刷新'
    ],
    'table' => [
        'id' => '编号',
        'name' => '键值',
        'display_name' => '名称',
        'action' => '操作'
    ],
    'modal' => [
        'name' => '键值',
        'display_name' => '名称',
        'close' => '关闭',
        'submit' => '提交',
    ],
    'feedback' => [
        'invalid_name' => '请填入正确键值',
        'invalid_display_name' => '请填入正确名称',
        'name_bad_chart' => '名称仅允许(A-Za-z0-9_)字元, 且不允许空白',
    ],
    'message' => [
        'field_required' => ':field 为必填栏位',
        'create_success' => '添加成功',
        'update_success' => '更新成功',
        'delete_success' => '删除成功',
        'delete_confirmed' => '确认删除该角色吗?',
        'delete_tip' => '删除后就不能复原',
        'confirm_text' => '是的',
        'cancel_text' => '取消',
        'delete_failed' => '删除失败',
        'delete_success' => '删除成功',
    ]
];