<?php

return [
    'nav' => [
        'general' => '用户管理',
        'system' => '系统',
        'cms' => '内容管理',
        'home' => '首頁',
        'logout' => '登出',
        'profile' => '个人资料',
    ],
    'profile' => [
        'title' => '个人资料'
    ],
    'actions' => [
        'success'             => '操作成功',
        'fail'                => '操作失败 (:message)',
        'delete_confirmed'    => '确认删除吗?',
        'delete_hint'         => '删除后无法复原！',
        'delete_confirm_text' => '是的',
        'delete_cancel_text'  => '取消',
    ]
];