<?php

return [
    'nav' => [
        'management' => '用戶管理',
        'create' => '新增用戶',
        'index' => '用戶查詢',
    ],
    'toolbar' => [
        'create' => '新增',
        'fresh' => '刷新'
    ],
    'toast' => [
        'error' => '錯誤'
    ],
    'modal' => [
        'title_create' => '新增用戶',
        'title_update' => '編輯用戶',
        'close' => '關閉',
        'submit' => '提交',
        'name' => '名稱',
        'email' => 'email',
        'password' => '密碼',
        'password_confirmation' => '密碼確認',
        'phone_number' => '手機號碼',
        'qq' => 'QQ',
        'role' => '角色',
        'chose_roles' => '請選擇角色',
        'password_tip' => '不更改密碼留空即可'
    ],
    'feedback' => [
        'invalid_name' => '請填入正確名稱',
        'invalid_email' => '請填入正確email格式',
        'invalid_password' => '密碼最少短長度爲8個字元',
        'invalid_password_confirmed' => '密碼確認與密碼不相同',
    ],
    'table' => [
        'id' => '編號',
        'name' => '名稱',
        'account' => '帳號',
        'email' => 'email',
        'phone_number' => '手機號碼',
        'created_at' => '新增時間',
        'action' => '操作',
    ],
    'message' => [
        'field_required' => ':field 為必填欄位',
        'password_different' => '密碼確認與密碼不相同',
        'access_denied' => '拒絕存取',
        'create_failed' => '新增失敗, 請聯繫維護人員',
        'create_invalid' => '新增失敗, 請確認填寫內容',
        'create_success' => '新增成功',
        'update_failed' => '更新失敗, 請聯繫維護人員',
        'update_invalid' => '更新失敗, 請確認填寫內容',
        'update_success' => '更新成功',
        'delete_confirmed' => '確認刪除該用戶嗎?',
        'delete_tip' => '刪除後就不能復原!',
        'delete_failed' => '刪除失敗, 請聯繫維護人員',
        'delete_success' => '刪除成功',
        'confirm_text' => '是的',
        'cancel_text' => '取消'
    ]
];