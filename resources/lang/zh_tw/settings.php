<?php

return [
    'button' => [
        'submit' => '更新',
        'upload' => '上傳'
    ],
    'nav' => [
        'index' => '參數設置'
    ],
    'toggle' => [
        'on' => '開啟',
        'off' => '關閉'
    ],
    'description' => [
        'site_title' => '站名',
        'site_description' => '網站描述',
        'site_keyword' => '網站關鍵字',
        'site_analysis_code' => '網站統計程式'
    ],
    'message' => [
        'update_success' => '更新成功'
    ]
];