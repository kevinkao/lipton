<?php

return [
    'role_create'     => '新增角色',
    'role_delete'     => '刪除角色',
    'role_edit'       => '編輯角色',
    'role_browse'     => '瀏覽/查看角色',
    'user_create'     => '新增用戶',
    'user_delete'     => '刪除用戶',
    'user_edit'       => '編輯用戶',
    'user_browse'     => '瀏覽/查看用戶',
    'settings_create' => '新增設置',
    'settings_delete' => '刪除設置',
    'settings_edit'   => '編輯設置',
    'settings_browse' => '瀏覽設置',
    'cms_category_create' => '新增欄目',
    'cms_category_delete' => '刪除欄目',
    'cms_category_edit' => '編輯欄目',
    'cms_category_browse' => '瀏覽/查看欄目',
    'cms_post_create' => '新增文章',
    'cms_post_delete' => '刪除文章',
    'cms_post_edit' => '編輯文章',
    'cms_post_browse' => '瀏覽/查看文章',
    'cms_crawler_create' => '新增爬蟲',
    'cms_crawler_delete' => '刪除爬蟲',
    'cms_crawler_edit' => '編輯爬蟲',
    'cms_crawler_browse' => '瀏覽爬蟲',
    'cms_comment_create' => '新增留言',
    'cms_comment_delete' => '刪除留言',
    'cms_comment_edit' => '編輯留言',
    'cms_comment_browse' => '瀏覽/查看留言',
];