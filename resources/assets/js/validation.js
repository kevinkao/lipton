/**
* Dependencies: validator
* https://www.npmjs.com/package/validator
*
* @author kevinkao@github.com
*/
(function ($) {
    $.extend(validator, {
        'required': function (input) {
            return validator.matches(input, /.+/);
        },
        'confirmed': function(input, selector) {
            return $(selector).val() == input;
        },
        'regexp': function(input, pattern) {
            if (/.+/.test(input)) {
                return pattern.test(input);
            }
            return true;
        }
    });

    $('.needs-validation').on('submit', function () {
        var invalidCount = 0;
        $('[verify]', this).each(function () {
            var $self = $(this);
            var rules = $self.attr('verify').split('|');
            var value = $self.is('input') ? $self.val() : this.value;
            $self.removeClass('is-invalid');
            $.each(rules, function (_, ruleName) {
                if (validator[ruleName].apply(validator, [value]) === false) {
                    $self.addClass('is-invalid');
                    invalidCount++;
                    return true;
                }
            });
        });

        if (invalidCount > 0) {
            return false;
        }
    });

    $.fn.validate = function(options, onSuccess, onError) {
        var self = this;
        $(self).data('options', options);
        var invalidCount = 0;
        $('input', self).each(function() {
            var rule = options[this.name];
            for (var ruleName in rule) {
                var args = [this.value].concat(rule[ruleName].args || []);
                if (validator[ruleName].apply(validator, args) === false) {
                    invalidCount++;
                    onError.apply(self, [this, ruleName, rule[ruleName].message]);
                    break;
                }
            }
        });

        if (invalidCount === 0) {
            onSuccess.apply(self, []);
        }
        return $(self);
    };

})(jQuery);