
@extends('lipton::layout')

@section('page_title', __('lipton::settings.nav.index'))

@push('styles')
<link rel="stylesheet" href="{{ Lipton::assets('/js/flatpickr/flatpickr.min.css') }}">
<link rel="stylesheet" href="{{ Lipton::assets('/bootstrap4-toggle/bootstrap4-toggle.min.css') }}">
<style type="text/css">
    h3.card-title {
        display: table;
        height: 2rem;
    }
    h3.card-title > .inner {
        display: table-cell;
        vertical-align: middle;
    }
</style>
@endpush

@section('container')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                @foreach($settings as $section)
                    @if ($section->type === 'text')
                        <div class="col-12">
                            <div class="card card-default">
                                <div class="overlay dark" style="display: none">
                                    <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                                </div>
                                <div class="card-header">
                                    <h3 class="card-title">
                                        <div class="inner">
                                            {{ __($section->display_name) }}
                                            <span style="color: grey">settings('{{ $section->key }}')</span>
                                        </div>
                                    </h3>
                                    @can('edit', 'settings')
                                    <div class="card-tools">
                                        <button class="submit btn btn-sm btn-primary">{{ __('lipton::settings.button.submit') }}</button>
                                    </div>
                                    @endcan
                                </div>
                                <div class="card-body">
                                    <input type="text" class="form-control" name="{{ $section->key }}" value="{{ $section->value }}">
                                </div>
                            </div>
                        </div>
                    @elseif($section->type === 'textarea')
                        <div class="col-12">
                            <div class="card card-default">
                                <div class="overlay dark" style="display: none">
                                    <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                                </div>
                                <div class="card-header">
                                    <h3 class="card-title">
                                        <div class="inner">
                                            {{ __($section->display_name) }}
                                            <span style="color: grey">settings('{{ $section->key }}')</span>
                                        </div>
                                    </h3>

                                    @can('edit', 'settings')
                                    <div class="card-tools">
                                        <button class="submit btn btn-sm btn-primary">{{ __('lipton::settings.button.submit') }}</button>
                                    </div>
                                    @endcan
                                </div>
                                <div class="card-body">
                                    <textarea name="{{ $section->key }}" class="form-control">{{ $section->value }}</textarea>
                                </div>
                            </div>
                        </div>
                    @elseif($section->type === 'datetime_picker')
                        <div class="col-12">
                            <div class="card card-default">
                                <div class="overlay dark" style="display: none">
                                    <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                                </div>
                                <div class="card-header">
                                    <h3 class="card-title">
                                        <div class="inner">
                                            {{ __($section->display_name) }}
                                            <span style="color: grey">settings('{{ $section->key }}')</span>
                                        </div>
                                    </h3>
                                    @can('edit', 'settings')
                                    <div class="card-tools">
                                        <button class="submit btn btn-sm btn-primary">{{ __('lipton::settings.button.submit') }}</button>
                                    </div>
                                    @endcan
                                </div>
                                <div class="card-body">
                                    <input type="text" name="{{ $section->key }}" value="{{ $section->value }}" class="datetime-picker form-control">
                                </div>
                            </div>
                        </div>
                    @elseif($section->type === 'date_picker')
                        <div class="col-12">
                            <div class="card card-default">
                                <div class="overlay dark" style="display: none">
                                    <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                                </div>
                                <div class="card-header">
                                    <h3 class="card-title">
                                        <div class="inner">
                                            {{ __($section->display_name) }}
                                            <span style="color: grey">settings('{{ $section->key }}')</span>
                                        </div>
                                    </h3>
                                    @can('edit', 'settings')
                                    <div class="card-tools">
                                        <button class="submit btn btn-sm btn-primary">{{ __('lipton::settings.button.submit') }}</button>
                                    </div>
                                    @endcan
                                </div>
                                <div class="card-body">
                                    <input type="text" name="{{ $section->key }}" value="{{ $section->value }}" class="date-picker form-control">
                                </div>
                            </div>
                        </div>
                    @elseif($section->type === 'time_picker')
                        <div class="col-12">
                            <div class="card card-default">
                                <div class="overlay dark" style="display: none">
                                    <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                                </div>
                                <div class="card-header">
                                    <h3 class="card-title">
                                        <div class="inner">
                                            {{ __($section->display_name) }}
                                            <span style="color: grey">settings('{{ $section->key }}')</span>
                                        </div>
                                    </h3>
                                    @can('edit', 'settings')
                                    <div class="card-tools">
                                        <button class="submit btn btn-sm btn-primary">{{ __('lipton::settings.button.submit') }}</button>
                                    </div>
                                    @endcan
                                </div>
                                <div class="card-body">
                                    <input type="text" name="{{ $section->key }}" value="{{ $section->value }}" class="time-picker form-control">
                                </div>
                            </div>
                        </div>
                    @elseif($section->type === 'editor')
                        <div class="col-12">
                            <div class="card card-default">
                                <div class="overlay dark" style="display: none">
                                    <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                                </div>
                                <div class="card-header">
                                    <h3 class="card-title">
                                        <div class="inner">
                                            {{ __($section->display_name) }}
                                            <span style="color: grey">settings('{{ $section->key }}')</span>
                                        </div>
                                    </h3>
                                    @can('edit', 'settings')
                                    <div class="card-tools">
                                        <button class="submit btn btn-sm btn-primary">{{ __('lipton::settings.button.submit') }}</button>
                                    </div>
                                    @endcan
                                </div>
                                <div class="card-body">
                                    <textarea class="tinymce-editor" name="{{ $section->key }}">{{ $section->value }}</textarea>
                                </div>
                            </div>
                        </div>
                    @elseif($section->type === 'toggle')
                        <div class="col-12">
                            <div class="card card-default">
                                <div class="overlay dark" style="display: none">
                                    <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                                </div>
                                <div class="card-header">
                                    <h3 class="card-title">
                                        <div class="inner">
                                            {{ __($section->display_name) }}
                                            <span style="color: grey">settings('{{ $section->key }}')</span>
                                        </div>
                                    </h3>
                                    @can('edit', 'settings')
                                    <div class="card-tools">
                                        <input
                                            type="checkbox"
                                            data-toggle="toggle"
                                            data-size="sm"
                                            name="{{ $section->key }}"
                                            {{ $section->value == 1 ? 'checked' : '' }}
                                            data-on="{{ __('lipton::settings.toggle.on') }}"
                                            data-off="{{ __('lipton::settings.toggle.off') }}">
                                    </div>
                                    @endcan
                                </div>
                            </div>
                        </div>
                    @elseif($section->type === 'image')
                        <div class="col-12">
                            <div class="card card-default">
                                <div class="overlay dark" style="display: none">
                                    <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                                </div>
                                <div class="card-header">
                                    <h3 class="card-title">
                                        <div class="inner">
                                            {{ __($section->display_name) }}
                                            <span style="color: grey">settings('{{ $section->key }}')</span>
                                        </div>
                                    </h3>
                                    @can('edit', 'settings')
                                    <div class="card-tools">
                                        <button class="upload btn btn-sm btn-primary">{{ __('lipton::settings.button.upload') }}</button>
                                    </div>
                                    @endcan
                                </div>

                                <div class="card-body">
                                    @can('edit', 'settings')
                                        <input
                                            name="{{ $section->key }}"
                                            class="image-upload"
                                            type="file"
                                            accept="image/*">
                                    @endcan
                                    <div class="img-preview" style="margin: .25rem 0">
                                        <img src="{{ $section->value }}" alt="" style="max-height: 16rem; max-width: 100%">
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
    </section>
@endsection

@push('scripts')
<script src="{{ Lipton::assets('/bootstrap4-toggle/bootstrap4-toggle.min.js') }}"></script>
<script src="{{ Lipton::assets('js/flatpickr/flatpickr.min.js') }}"></script>
<script src="{{ Lipton::assets('js/flatpickr/l10n/zh.js') }}"></script>
<script src="{{ Lipton::assets('js/tinymce/tinymce-4.9.2.min.js') }}"></script>
@if (App::isLocale('zh_tw'))
<script src="{{ Lipton::assets('js/tinymce/lang/zh_tw.js') }}"></script>
@elseif(App::isLocale('zh_cn'))
<script src="{{ Lipton::assets('js/tinymce/lang/zh_cn.js') }}"></script>
@endif
<script>
    $('.submit').on('click', function() {
        var self = this;
        var $card = $(self).parents('.card');
        $('.overlay', $card).css('display', 'flex');
        var $input = $('input, textarea', $card);
        if ($input.is('textarea')) {
            tinymce.triggerSave();
        }
        var data = {
            key: $input.attr('name'),
            value: $input.val()
        };
        $.ajax({
            url: '{{ route('lipton.settings.update') }}',
            type: 'put',
            data: JSON.stringify(data),
            headers: {
                "Content-type": 'application/json'
            }
        }).done(function(resp) {
            if (resp.code > 0) {
                Toast.fire({
                    type: 'warning',
                    title: resp.message
                });
                return false;
            }
            Toast.fire({
                type: 'success',
                title: '{{ __('lipton::common.actions.success') }}'
            });
        }).always(function() {
            $('.overlay', $card).css('display', 'none');
        });
    });
    $('[data-toggle=toggle]').on('change', function() {
        var self = this;
        var $card = $(self).parents('.card');
        var data = {
            key: $(self).attr('name'),
            value: $(self).prop('checked') ? 1 : 0
        };
        $('.overlay', $card).css('display', 'flex');
        $.ajax({
            url: '{{ route('lipton.settings.update') }}',
            type: 'put',
            data: JSON.stringify(data),
            headers: {
                "Content-type": 'application/json'
            }
        }).done(function(resp) {
            if (resp.code > 0) {
                Toast.fire({
                    type: 'warning',
                    title: resp.message
                });
                return false;
            }
            Toast.fire({
                type: 'success',
                title: '{{ __('lipton::common.actions.success') }}'
            });
        }).always(function() {
            $('.overlay', $card).css('display', 'none');
        });
    });

    $('.image-upload').on('change', function() {
        var self = this;
        if (this.files && this.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $(self).next('.img-preview')
                    .find('img')
                    .attr('src', e.target.result);
            }    
            reader.readAsDataURL(this.files[0]); // convert to base64 string
        }
    });

    $('.upload').on('click', function() {
        var self = this;
        var $card = $(self).parents('.card');
        $('.overlay', $card).css('display', 'flex');

        var formData = new FormData();
        var $input = $card.find('.image-upload');
        formData.append('key', $input.attr('name'));
        formData.append('image', $input.prop('files')[0]);

        $.ajax({
            url: '{{ route('lipton.settings.upload') }}',
            type: 'post',
            data: formData,
            processData: false,
            contentType: false
        }).done(function(resp) {
            if (resp.code > 0) {
                Toast.fire({
                    type: 'warning',
                    title: resp.message
                });
                return false;
            }

            $card.find('.img-preview')
                .find('img')
                .attr('src', resp.data.value);

            $input.val('');

            Toast.fire({
                type: 'success',
                title: '{{ __('lipton::common.actions.success') }}'
            });
        }).always(function() {
            $('.overlay', $card).css('display', 'none');
        });
    });

    $('.datetime-picker').flatpickr({
        enableTime: true,
        dateFormat: 'Y-m-d H:i',
        time_24hr: true,
        locale: 'zh'
    });
    $('.date-picker').flatpickr({
        enableTime: false,
        dateFormat: 'Y-m-d',
        time_24hr: true,
        locale: 'zh'
    });
    $('.time-picker').flatpickr({
        enableTime: true,
        noCalendar: true,
        dateFormat: 'H:i',
        time_24hr: true,
        locale: 'zh'
    });
    tinymce.init({
        selector:'.tinymce-editor',
        plugins: 'table link preview textcolor colorpicker image lists advlist fullpage fullscreen pagebreak nonbreaking',
        toolbar: "undo redo | fullscreen preview | image | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link | table | pagebreak | forecolor backcolor4 | fontsizeselect",
        height: '400',
        relative_urls : false,
        convert_urls : false,
        file_picker_types: 'image',
        images_upload_handler: function (blobInfo, success, failure) {
            var xhr, formData;
            xhr = new XMLHttpRequest();
            xhr.withCredentials = false;
            xhr.open('POST', '{{ route('lipton.cms.post.editorUpload') }}');
            var token = $('meta[name="csrf-token"]').attr('content');
            xhr.setRequestHeader("X-CSRF-Token", token);
            xhr.onload = function () {
                var json;
                if (xhr.status != 200) {
                    failure('HTTP Error: ' + xhr.status);
                    return;
                }
                json = JSON.parse(xhr.responseText);
                if (json.code > 0) {
                    return failure(json.message);
                }

                if (!json || typeof json.src != 'string') {
                    failure('Invalid JSON: ' + xhr.responseText);
                    return;
                }

                success(json.src);
            };
            formData = new FormData();
            formData.append('file', blobInfo.blob(), blobInfo.filename());
            xhr.send(formData);
        }
    });
</script>
@endpush