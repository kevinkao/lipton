<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="{{ settings('site_description') }}">
    <meta name="keywords" content="{{ settings('site_keyword') }}">
    <title>{{ settings('site_title') }}</title>
    @stack('pre-styles')
    <link rel="stylesheet" href="{{ Lipton::assets('/admin-lte/plugins/fontawesome-free/css/all.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ Lipton::assets('/admin-lte/plugins/ionicons/ionicons.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ Lipton::assets('/admin-lte/dist/css/adminlte.min.css') }}">
    <!-- overlayScrollbars -->
    {{-- <link rel="stylesheet" href="{{ Lipton::assets('/admin-lte/plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}"> --}}
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <link rel="stylesheet" href="{{ Lipton::assets('/admin-lte/plugins/sweetalert2/sweetalert2.min.css') }}">
    <!-- loading animation -->
    <link rel="stylesheet" href="{{ Lipton::assets('/js/animsition/animsition.min.css') }}"></link>
    <style>
        .modal-content > .overlay {
            position: absolute;
            width: 100%;
            height: 100%;
            background: #000;
            opacity: 0.6;
            z-index: 1;
            align-items: center;
            justify-content: center;
            border-radius: .3rem;
        }
        .modal-content > .overlay.shown {
            display: flex!important;
        }
        .modal-content > .overlay.hidden {
            display: none!important;
        }
        .modal-content > .overlay > .fas {
            color: #ced4da;
            animation: 2s linear 0s normal none infinite spin;
        }
        @keyframes spin {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(359deg); }
        }
        table .action {
            margin-right: .3rem;
        }
        .animsition-overlay-slide {
            z-index: 500;
        }
        .toolbar {
            width: 100%;
        }
        /* for tinymce fullscreen */
        div.mce-fullscreen {
            z-index: 1050;
        }
    </style>
    @stack('styles')
</head>
<body class="hold-transition sidebar-mini ">
    <div
        class="animsition-overlay"
        data-animsition-loading="true"
        data-animsition-loading-class="animsition-loading"
        data-animsition-overlay="true"
        data-animsition-in-class="overlay-slide-in-top"
        data-animsition-in-duration="1000"
        data-animsition-out-class="overlay-slide-out-top"
        data-animsition-out-duration="800">

        <div class="wrapper">
            
            @component('lipton::nav')
            @endcomponent

            <!-- Main Sidebar Container -->
            @component('lipton::sidebar')
            @endcomponent

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <div class="container-fluid">
                      <div class="row mb-2">
                          <div class="col-sm-8">
                              @yield('page_toolbar')
                          </div>
                          <div class="col-sm-4">
                              @yield('page_breadcrumb')
                          </div>
                      </div><!-- /.row -->
                    </div><!-- /.container-fluid -->
                </section>
                <!-- /.content-header -->

                <!-- Main content -->
                <section class="content">
                    <div class="container-fluid">
                        @yield('container')
                    </div><!-- /.container-fluid -->
                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->

            @component('lipton::footer')
            @endcomponent

            <!-- Control Sidebar -->
            <aside class="control-sidebar control-sidebar-dark">
              <!-- Control sidebar content goes here -->
            </aside>
            <!-- /.control-sidebar -->
        </div>
    </div>

    <!-- jQuery -->
    <script src="{{ Lipton::assets('/admin-lte/plugins/jquery/jquery.min.js') }}"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="{{ Lipton::assets('/admin-lte/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script src="{{ Lipton::assets('/js/animsition/animsition.min.js') }}"></script>
    <script>
        $.widget.bridge('uibutton', $.ui.button)
    </script>
    <!-- Bootstrap 4 -->
    <script src="{{ Lipton::assets('/admin-lte/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ Lipton::assets('/admin-lte/dist/js/adminlte.js') }}"></script>
    <script src="{{ Lipton::assets('/admin-lte/dist/js/demo.js') }}"></script>
    <script src="{{ Lipton::assets('/admin-lte/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
    <script>
        $('[data-toggle="tooltip"]').tooltip();
        $.ajaxSetup({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('a:not([href="#"])').not('[data-widget], [data-toggle="dropdown"], .nav-tabs a').addClass('animsition-link');
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000,
        });
        $('.animsition-overlay').animsition({
            transition: function(url) {
                if (!url) {
                    return;
                }
                window.location.href = url;
            }
        });
    </script>
    @stack('scripts')
</body>
</html>