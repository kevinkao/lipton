
@extends('lipton::layout')

@push('pre-styles')
<link rel="stylesheet" href="{{ Lipton::assets('/admin-lte/plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
<link rel="stylesheet" href="{{ Lipton::assets('/admin-lte/plugins/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ Lipton::assets('/admin-lte/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
<style>
    table .action {
        margin-right: .3rem;
    }
</style>
@endpush

@section('page_toolbar')
<div class="toolbar">
    <button class="btn btn-success" id="fresh-table">{{ __('lipton::user.toolbar.fresh') }}</button>
    @can('create', 'user')
    <button class="btn btn-primary create-user" data-toggle="modal" data-target="#user-create-modal">{{ __('lipton::user.toolbar.create') }}</button>
    @endcan
</div>
@endsection

@section('page_breadcrumb')
    <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="{{ route(config('lipton.default_route')) }}">{{ __('lipton::common.nav.home') }}</a></li>
        <li class="breadcrumb-item active">{{ __('lipton::user.nav.index') }}</li>
    </ol>
@endsection

@section('container')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <table id="datatable" class="table table-bordered table-striped">
                    <thead>
                        <th>{{ __('lipton::user.table.id') }}</th>
                        <th>{{ __('lipton::user.table.name') }}</th>
                        <th>{{ __('lipton::user.table.account') }}</th>
                        <th>{{ __('lipton::user.table.email') }}</th>
                        <th>{{ __('lipton::user.table.phone_number') }}</th>
                        <th>{{ __('lipton::user.table.action') }}</th>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
    
    <div class="modal fade user-modal" id="user-create-modal" tabindex="-1" role="dialog" aria-labelledby="create-form-label" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="overlay hidden">
                    <i class="fas fa-2x fa-sync-alt"></i>
                </div>
                <div class="modal-header">
                    <h5 class="modal-title" id="create-form-label">{{ __('lipton::user.modal.title_create') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <label for="">{{ __('lipton::user.modal.name') }}<span style="color: red">*</span></label>
                            <input name="name" type="text" class="form-control">
                            <div class="invalid-feedback"></div>
                        </div>
                        <div class="form-group">
                            <label for="">{{ __('lipton::user.modal.email') }}<span style="color: red">*</span></label>
                            <input name="email" type="email" class="form-control">
                            <div class="invalid-feedback"></div>
                        </div>
                        <div class="form-group">
                            <label for="">{{ __('lipton::user.modal.password') }}<span style="color: red">*</span></label>
                            <input name="password" type="password" class="form-control">
                            <div class="invalid-feedback"></div>
                        </div>
                        <div class="form-group">
                            <label for="">{{ __('lipton::user.modal.password_confirmation') }}<span style="color: red">*</span></label>
                            <input name="password_confirmation" type="password" class="form-control">
                            <div class="invalid-feedback"></div>
                        </div>
                        <div class="form-group">
                            <label for="">{{ __('lipton::user.modal.phone_number') }}</label>
                            <input name="phone_number" type="text" class="form-control">
                            <div class="invalid-feedback"></div>
                        </div>
                        <div class="form-group">
                            <label for="">{{ __('lipton::user.modal.qq') }}</label>
                            <input name="qq" type="text" class="form-control">
                            <div class="invalid-feedback"></div>
                        </div>
                        <div class="form-group">
                            <label for="">{{ __('lipton::user.modal.role') }}</label>
                            <select class="select2" multiple="multiple" data-placeholder="選擇角色" style="width: 100%;">
                                @foreach($roles as $role)
                                    <option value="{{ $role->id }}">{{ __($role->display_name) }}</option>>
                                @endforeach
                            </select>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('lipton::user.modal.close') }}</button>
                    <button type="button" class="btn btn-primary">{{ __('lipton::user.modal.submit') }}</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade user-modal" id="user-update-modal" tabindex="-1" role="dialog" aria-labelledby="create-form-label" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="overlay hidden">
                    <i class="fas fa-2x fa-sync-alt"></i>
                </div>
                <div class="modal-header">
                    <h5 class="modal-title" id="create-form-label">{{ __('lipton::user.modal.title_update') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <label for="">{{ __('lipton::user.modal.name') }}</label>
                            <input name="name" type="text" class="form-control">
                            <div class="invalid-feedback"></div>
                        </div>
                        <div class="form-group">
                            <label for="">{{ __('lipton::user.modal.password') }}</label>
                            <input name="password" type="password" class="form-control" placeholder="{{ __('lipton::user.modal.password_tip') }}">
                            <div class="invalid-feedback"></div>
                        </div>
                        <div class="form-group">
                            <label for="">{{ __('lipton::user.modal.password_confirmation') }}</label>
                            <input name="password_confirmation" type="password" class="form-control" placeholder="{{ __('lipton::user.modal.password_tip') }}">
                            <div class="invalid-feedback"></div>
                        </div>
                        <div class="form-group">
                            <label for="">{{ __('lipton::user.modal.phone_number') }}</label>
                            <input name="phone_number" type="text" class="form-control">
                            <div class="invalid-feedback"></div>
                        </div>
                        <div class="form-group">
                            <label for="">{{ __('lipton::user.modal.qq') }}</label>
                            <input name="qq" type="text" class="form-control">
                            <div class="invalid-feedback"></div>
                        </div>
                        <div class="form-group">
                            <label for="">{{ __('lipton::user.modal.role') }}</label>
                            <select id="roles-select" class="select2" multiple="multiple" data-placeholder="{{ __('lipton::user.modal.chose_roles') }}" style="width: 100%;">
                                @foreach($roles as $role)
                                    <option value="{{ $role->id }}">{{ __($role->display_name) }}</option>>
                                @endforeach
                            </select>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('lipton::user.modal.close') }}</button>
                    <button type="button" class="btn btn-primary">{{ __('lipton::user.modal.submit') }}</button>
                </div>
            </div>
        </div>
    </div>
</div>  
@endsection

@push('scripts')
<!-- DataTables -->
<script src="{{ Lipton::assets('/admin-lte/plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ Lipton::assets('/admin-lte/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
<script src="{{ Lipton::assets('/admin-lte/plugins/select2/js/select2.full.min.js') }}"></script>
<script src="{{ Lipton::assets('/js/validator.min.js') }}"></script>
<script src="{{ Lipton::assets('/js/validation.js') }}"></script>
@datatable_locale()
<script>
    $('#fresh-table').on('click', function() {
        $('#datatable').DataTable().ajax.reload(null, false);
    });
    $('.user-modal')
        .on('hidden.bs.modal', function() {
            $('input', this).val('').removeClass('is-invalid');
            $('select.select2').val([]).trigger('change');
        });

    $('#user-update-modal')
        .on('click', '.btn-primary', function(e) {
            var $form = $(this).parents('.modal-content').find('form');
            var userId = $(this).data('id');
            $form.find('.is-invalid').removeClass('is-invalid');
            $form.validate({
                name: {
                    isLength: {
                        args: [{ min: 1 }],
                        message: "{{ __('lipton::user.feedback.invalid_name') }}"
                    }
                },
                password_confirmation: {
                    confirmed: {
                        // jQuery selector
                        args: ['#user-update-modal input[name="password"]'],
                        message: "{{ __('lipton::user.feedback.invalid_password_confirmed') }}"
                    }
                }
            }, function() {
                // Valid
                var self = this;
                var data = {};
                $form.find('input.form-control').each(function() {
                    if (this.value.length > 0) {
                        data[this.name] = this.value;    
                    }
                });
                data.roles = $form.find('select').val();

                $('.overlay', '#user-update-modal').removeClass('hidden').addClass('shown');

                $.ajax({
                    url: '{{ route('lipton.user.update', ':id') }}'.replace(':id', userId),
                    type: 'put',
                    data: JSON.stringify(data),
                    dataType: 'json',
                    headers: {
                        'Content-type': 'application/json'
                    }
                }).done(function(resp) {
                    switch (resp.code) {
                        case 1:
                            Toast.fire({
                                type: 'error',
                                title: '{{ __('lipton::user.message.update_failed') }}'
                            });
                            return false;

                        case 2:
                            Toast.fire({
                                type: 'warning',
                                title: '{{ __('lipton::user.message.update_invalid') }}'
                            });
                            return false;
                    }
                    $('#datatable').DataTable().ajax.reload(null, false);
                    Toast.fire({
                        type: 'success',
                        title: '{{ __('lipton::user.message.update_success') }}'
                    });
                    $('#user-update-modal').modal('hide');
                }).always(function() {
                    $('.overlay', '#user-update-modal').removeClass('shown').addClass('hidden');
                });
            }, function(ele, ruleName, message) {
                // Invalid
                $(ele).addClass('is-invalid').siblings('.invalid-feedback').html(message);
            });
        }).on('show.bs.modal', function(e) {
            var $button = $(e.relatedTarget);
            var data = $button.parents('tr').data();
            $('#user-update-modal input[name=name]').val(data.name);
            $('#user-update-modal input[name=phone_number]').val(data.phone_number);
            $('#user-update-modal input[name=qq]').val(data.qq);
            var id = [];
            $.each(data.roles, function(_, item) {
                id.push(item.id);
            });
            $('#roles-select').val(id).trigger('change');
            $('#user-update-modal .btn-primary').data('id', $button.data('id'));
        });

    $('#user-create-modal')
        .on('click', '.btn-primary', function() {
            var $form = $(this).parents('.modal-content').find('form');
            $form.find('.is-invalid').removeClass('is-invalid');
            $form.validate({
                name: {
                    isLength: {
                        args: [{ min: 1 }],
                        message: "{{ __('lipton::user.feedback.invalid_name') }}"
                    }
                },
                email: {
                    isEmail: {
                        message: "{{ __('lipton::user.feedback.invalid_email') }}"
                    }
                },
                password: {
                    isLength: {
                        args: [{ min: 6 }],
                        message: "{{ __('lipton::user.feedback.invalid_password') }}"
                    }
                },
                password_confirmation: {
                    confirmed: {
                        // jQuery selector
                        args: ['#user-create-modal input[name="password"]'],
                        message: "{{ __('lipton::user.feedback.invalid_password_confirmed') }}"
                    }
                }
            }, function() {
                // Valid
                var self = this;
                var data = {};
                $form.find('input.form-control').each(function() {
                    data[this.name] = this.value;
                });
                data.roles = $form.find('select').val();

                $('.overlay', '#user-create-modal').removeClass('hidden').addClass('shown');

                $.ajax({
                    url: '{{ route('lipton.user.store') }}',
                    type: 'post',
                    data: JSON.stringify(data),
                    dataType: 'json',
                    headers: {
                        'Content-type': 'application/json'
                    }
                }).done(function(resp) {
                    switch (resp.code) {
                        case 1:
                            Toast.fire({
                                type: 'error',
                                title: '{{ __('lipton::user.message.create_failed') }}'
                            });
                            return false;

                        case 2:
                            Toast.fire({
                                type: 'warning',
                                title: '{{ __('lipton::user.message.create_invalid') }}'
                            });
                            return false;
                    }
                    $('#datatable').DataTable().ajax.reload(null, false);
                    Toast.fire({
                        type: 'success',
                        title: '{{ __('lipton::user.message.create_success') }}'
                    });
                    $('#user-create-modal').modal('hide');
                }).always(function() {
                    $('.overlay', '#user-create-modal').removeClass('shown').addClass('hidden');
                });
            }, function(ele, ruleName, message) {
                // Invalid
                $(ele).addClass('is-invalid').siblings('.invalid-feedback').html(message);
            });
        });

    $('.select2').select2();
    $('#datatable').DataTable({
        "autoWidth": false,
        "serverSide": true,
        "processing": true,
        "stateSave": true,
        "ajax": {
            url: '{{ route('lipton.user.datatable') }}',
            type: 'post'
        },
        "createdRow": function(row, data, dataIndex) {
            $(row).data(data);
        },
        "columns": [
            { "data": "id" },
            { "data": "name" },
            { "data": "account" },
            { "data": "email" },
            { "data": "phone_number" },
            { "data": "actions" }
        ]
    }).on('click', '.delete', function() {
        var id = $(this).data('id');
        Swal.fire({
            title: '{{ __('lipton::user.message.delete_confirmed') }}',
            text: "{{ __('lipton::user.message.delete_tip') }}",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: '{{ __('lipton::user.message.confirm_text') }}',
            cancelButtonText: '{{ __('lipton::user.message.cancel_text') }}'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: '{{ route('lipton.user.destroy', ':id') }}'.replace(':id', id),
                    type: 'delete'
                }).done(function(resp) {
                    if (resp.code > 0) {
                        Toast.fire({
                            type: 'error',
                            title: '{{ __('lipton::user.message.delete_failed') }}'
                        });
                        return;
                    }
                    $('#datatable').DataTable().ajax.reload(null, false);
                    Toast.fire({
                        type: 'success',
                        title: '{{ __('lipton::user.message.delete_success') }}'
                    });
                });
            }
        });
    });
</script>
@endpush