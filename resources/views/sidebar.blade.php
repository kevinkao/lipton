<aside class="main-sidebar sidebar-dark-primary elevation-4">
  <!-- Brand Logo -->
  <a href="#" class="brand-link">
    <img src="{{ Lipton::assets('/admin-lte/dist/img/AdminLTELogo.png') }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
         style="opacity: .8">
    <span class="brand-text font-weight-light">AdminLTE 3</span>
  </a>

  <!-- Sidebar -->
  <div class="sidebar">
    <!-- Sidebar user panel (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        <img src="https://ui-avatars.com/api/?size=160&name={{ auth()->user()->name }}" class="img-circle elevation-2" alt="User Image">
      </div>
      <div class="info">
        <a href="#" class="d-block">{{ auth()->user()->name }}</a>
      </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        @foreach($nav as $header => $items)
          <li class="nav-header">{{ __($header) }}</li>
          @foreach($items as $item)
            @if ($item->hasTreeView)
              <li class="nav-item has-treeview {{ $item->isActivited() ? 'menu-open' : '' }}">
                <a href="#" class="nav-link {{ $item->isActivited() ? 'active' : '' }}">
                  <i class="nav-icon fas {{ $item->iconClass }}"></i>
                  <p>{{ __($item->title) }}
                    <i class="right fas fa-angle-left"></i>
                    {{-- <span class="badge badge-info right">2</span> --}}
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  @foreach($item->getChildItems() as $childItem)
                    @if ($childItem->isVisible())
                    <li class="nav-item">
                      <a href="{{ $childItem->getLink() }}" class="nav-link {{ $childItem->isActivited() ? 'active' : '' }}">
                        <i class="fas {{ $childItem->iconClass }} nav-icon"></i>
                        <p>{{ __($childItem->title) }}</p>
                      </a>
                    </li>
                    @endif
                  @endforeach
                </ul>
              </li>
            @else
              @if ($item->isVisible())
              <li class="nav-item">
                  <a href="{{ $item->getLink() }}" class="nav-link {{ $item->isActivited() ? 'active' : '' }}">
                    <i class="nav-icon fas {{ $item->iconClass }}"></i>
                    <p>
                      {{ __($item->title) }}
                      {{-- <span class="badge badge-info right">2</span> --}}
                    </p>
                  </a>
              </li>
              @endif
            @endif
          @endforeach
        @endforeach
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>