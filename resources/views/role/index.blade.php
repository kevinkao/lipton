@extends('lipton::layout')

@push('pre-styles')
<link rel="stylesheet" href="{{ Lipton::assets('/admin-lte/plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
<link rel="stylesheet" href="{{ Lipton::assets('/admin-lte/plugins/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ Lipton::assets('/admin-lte/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ Lipton::assets('/admin-lte/plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css') }}">
@endpush

@section('page_toolbar')
<div class="toolbar">
    <button class="btn btn-success" id="fresh-table">{{ __('lipton::role.toolbar.fresh') }}</button>
    @can('create', 'role')
    <button class="btn btn-primary create-role" data-toggle="modal" data-target="#role-create-modal">{{ __('lipton::role.toolbar.create') }}</button>
    @endcan
</div>
@endsection

@section('page_breadcrumb')
    <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="{{ route(config('lipton.default_route')) }}">{{ __('lipton::common.nav.home') }}</a></li>
        <li class="breadcrumb-item active">{{ __('lipton::role.nav.index') }}</li>
    </ol>
@endsection

@section('container')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <table id="datatable" class="table table-bordered table-striped">
                        <thead>
                            <th>{{ __('lipton::role.table.id') }}</th>
                            <th>{{ __('lipton::role.table.name') }}</th>
                            <th>{{ __('lipton::role.table.display_name') }}</th>
                            <th>{{ __('lipton::role.table.action') }}</th>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade role-modal" id="role-create-modal" tabindex="-1" role="dialog" aria-labelledby="create-form-label" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="overlay hidden">
                    <i class="fas fa-2x fa-sync-alt"></i>
                </div>
                <div class="modal-header">
                    <h5 class="modal-title" id="create-form-label">{{ __('lipton::user.modal.title_create') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <label for="">{{ __('lipton::role.modal.name') }}</label>
                            <input name="name" type="text" class="form-control">
                            <div class="invalid-feedback"></div>
                        </div>
                        <div class="form-group">
                            <label for="">{{ __('lipton::role.modal.display_name') }}</label>
                            <input name="display_name" type="text" class="form-control">
                            <div class="invalid-feedback"></div>
                        </div>
                        <div class="form-group">
                            <label for="">{{ __('lipton::user.modal.role') }}</label>
                            <select class="duallistbox" multiple="multiple" name="duallistbox1[]">
                                @foreach($permissions as $permission)
                                    <option value="{{ $permission->id }}">{{ __($permission->display_name) }}</option>>
                                @endforeach
                            </select>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('lipton::role.modal.close') }}</button>
                    <button type="button" class="btn btn-primary">{{ __('lipton::role.modal.submit') }}</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade role-modal" id="role-update-modal" tabindex="-1" role="dialog" aria-labelledby="update-form-label" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="overlay hidden">
                    <i class="fas fa-2x fa-sync-alt"></i>
                </div>
                <div class="modal-header">
                    <h5 class="modal-title" id="update-form-label">{{ __('lipton::user.modal.title_update') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <label for="">{{ __('lipton::role.modal.display_name') }}</label>
                            <input name="display_name" type="text" class="form-control">
                            <div class="invalid-feedback"></div>
                        </div>
                        <div class="form-group">
                            <label for="">{{ __('lipton::user.modal.role') }}</label>
                            <select class="duallistbox" multiple="multiple" name="duallistbox2[]">
                                @foreach($permissions as $permission)
                                    <option value="{{ $permission->id }}">{{ __($permission->display_name) }}</option>>
                                @endforeach
                            </select>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('lipton::role.modal.close') }}</button>
                    <button type="button" class="btn btn-primary">{{ __('lipton::role.modal.submit') }}</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<!-- DataTables -->
<script src="{{ Lipton::assets('/admin-lte/plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ Lipton::assets('/admin-lte/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
<script src="{{ Lipton::assets('/admin-lte/plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js') }}"></script>
<script src="{{ Lipton::assets('/js/validator.min.js') }}"></script>
<script src="{{ Lipton::assets('/js/validation.js') }}"></script>
@datatable_locale()
<script>
    $('#fresh-table').on('click', function() {
        $('#datatable').DataTable().ajax.reload(null, false);
    });
    $('.role-modal').on('hidden.bs.modal', function() {
        $('input', this).val('');
        $('select', this).val([]).bootstrapDualListbox('refresh', true);
    });
    $('#role-create-modal')
        .on('click', '.btn-primary', function() {
            var $form = $(this).parents('.modal-content').find('form');
            $form.find('.is-invalid').removeClass('is-invalid');
            $form.validate({
                name: {
                    isLength: {
                        args: [{ min: 1, max: 24 }],
                        message: "{{ __('lipton::role.feedback.invalid_name') }}"
                    },
                    regexp: {
                        args: [/^\w+$/],
                        message: "{{ __('lipton::role.feedback.name_bad_chart') }}"
                    }
                },
                display_name: {
                    isLength: {
                        args: [{ min: 1, max: 24 }],
                        message: "{{ __('lipton::role.feedback.invalid_display_name') }}"
                    }
                }
            }, function() {
                var form = this;
                var data = {};
                $('input.form-control', form).each(function(_, ele) {
                    data[this.name] = this.value;
                });
                data['permissions'] = $('select[name="duallistbox1[]"]', form).val();
                $('.overlay', '#role-create-modal').removeClass('hidden').addClass('shown');
                $.ajax({
                    url: '{{ route('lipton.role.store') }}',
                    type: 'post',
                    dataType: 'json',
                    data: JSON.stringify(data),
                    headers: {
                        'Content-type': 'application/json'
                    }
                }).done(function(resp) {
                    if (resp.code > 0) {
                        Toast.fire({
                            type: 'error',
                            title: '{{ __('lipton::user.message.create_failed') }}'
                        });
                        return false;
                    }
                    $('#datatable').DataTable().ajax.reload(null, false);
                    Toast.fire({
                        type: 'success',
                        title: '{{ __('lipton::role.message.create_success') }}'
                    });
                    $('#role-create-modal').modal('hide');
                }).always(function() {
                    $('.overlay', '#role-create-modal').removeClass('shown').addClass('hidden');
                });
            }, function(ele, ruleName, message) {
                $(ele).addClass('is-invalid').siblings('.invalid-feedback').html(message);
            });
        });
    $('#role-update-modal')
        .on('click', '.btn-primary', function() {
            var $form = $(this).parents('.modal-content').find('form');
            var roleId = $(this).data('id');
            $form.find('.is-invalid').removeClass('is-invalid');
            $form.validate({
                display_name: {
                    isLength: {
                        args: [{ min: 1, max: 24 }],
                        message: "{{ __('lipton::role.feedback.invalid_display_name') }}"
                    }
                }
            }, function() {
                var form = this;
                var data = {};
                data['display_name'] = $('input[name="display_name"]', form).val();
                data['permissions'] = $('select[name="duallistbox2[]"]', form).val();
                $('.overlay', '#role-update-modal').removeClass('hidden').addClass('shown');
                $.ajax({
                    url: '{{ route('lipton.role.update', ':id') }}'.replace(':id', roleId),
                    type: 'put',
                    dataType: 'json',
                    data: JSON.stringify(data),
                    headers: {
                        'Content-type': 'application/json'
                    }
                }).done(function(resp) {
                    if (resp.code > 0) {
                        Toast.fire({
                            type: 'error',
                            title: '{{ __('lipton::user.message.create_failed') }}'
                        });
                        return false;
                    }
                    $('#datatable').DataTable().ajax.reload(null, false);
                    Toast.fire({
                        type: 'success',
                        title: '{{ __('lipton::role.message.update_success') }}'
                    });
                    $('#role-update-modal').modal('hide');
                }).always(function() {
                    $('.overlay', '#role-update-modal').removeClass('shown').addClass('hidden');
                });
            }, function(ele, ruleName, message) {
                $(ele).addClass('is-invalid').siblings('.invalid-feedback').html(message);
            });
        })
        .on('show.bs.modal', function(e) {
            var $button = $(e.relatedTarget);
            var data = $button.parents('tr').data();
            $('input[name="display_name"]', '#role-update-modal').val(data.display_name);
            var id = [];
            $.each(data.permissions, function(_, item) {
                id.push(item.id);
            });
            $('select', '#role-update-modal').val(id).bootstrapDualListbox('refresh', true);
            $('#role-update-modal .btn-primary').data('id', $button.data('id'));
        });
    $('select.duallistbox').bootstrapDualListbox({
        selectorMinimalHeight: 180
    });
    $('#datatable').DataTable({
        "autoWidth": false,
        "serverSide": true,
        "processing": true,
        "stateSave": true,
        "ajax": {
            url: '{{ route('lipton.role.datatable') }}',
            type: 'post'
        },
        "createdRow": function(row, data, dataIndex) {
            $(row).data(data);
        },
        "columns": [
            { "data": "id" },
            { "data": "name" },
            { "data": "display_name" },
            { "data": "actions" }
        ]
    }).on('click', '.delete', function() {
        var id = $(this).data('id');
        Swal.fire({
            title: '{{ __('lipton::role.message.delete_confirmed') }}',
            text: "{{ __('lipton::role.message.delete_tip') }}",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: '{{ __('lipton::role.message.confirm_text') }}',
            cancelButtonText: '{{ __('lipton::role.message.cancel_text') }}'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: '{{ route('lipton.role.destroy', ':id') }}'.replace(':id', id),
                    type: 'delete'
                }).done(function(resp) {
                    if (resp.code > 0) {
                        Toast.fire({
                            type: 'error',
                            title: '{{ __('lipton::role.message.delete_failed') }}'
                        });
                        return;
                    }
                    $('#datatable').DataTable().ajax.reload(null, false);
                    Toast.fire({
                        type: 'success',
                        title: '{{ __('lipton::role.message.delete_success') }}'
                    });
                });
            }
        });
    });
</script>
@endpush