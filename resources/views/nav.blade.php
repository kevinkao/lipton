<!-- Navbar -->
<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
            <a href="{{ route(config('lipton.default_route')) }}" class="nav-link">{{ __('lipton::common.nav.home') }}</a>
        </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
        @stack('pre-right-navbar')
        <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown" href="#" aria-expanded="true">
                <i class="fas fa-user"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                <div class="dropdown-divider"></div>
                <a href="#" class="dropdown-item" data-toggle="modal" data-target="#profile-modal">
                    <i class="fas fa-user-edit mr-2"></i> {{ __('lipton::common.nav.profile') }}
                </a>
                <div class="dropdown-divider"></div>
                <a href="{{ route('lipton.logout') }}" class="dropdown-item">
                    <i class="fas fa-door-open mr-2"></i> {{ __('lipton::common.nav.logout') }}
                </a>
            </div>
        </li>
        @stack('post-right-navbar')
    </ul>
</nav>
<!-- /.navbar -->

<div class="modal fade" id="profile-modal" tabindex="-1" role="dialog" aria-labelledby="create-form-label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="overlay hidden">
                <i class="fas fa-2x fa-sync-alt"></i>
            </div>
            <div class="modal-header">
                <h5 class="modal-title" id="create-form-label">{{ __('lipton::common.profile.title') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="">{{ __('lipton::user.modal.email') }}</label>
                        <input name="email" value="" type="text" class="form-control" disabled="true">
                        <div class="invalid-feedback"></div>
                    </div>
                    <div class="form-group">
                        <label for="">{{ __('lipton::user.modal.name') }}</label>
                        <input name="name" value="" type="text" class="form-control">
                        <div class="invalid-feedback"></div>
                    </div>
                    <div class="form-group">
                        <label for="">{{ __('lipton::user.modal.password') }}</label>
                        <input name="password" type="password" class="form-control" placeholder="{{ __('lipton::user.modal.password_tip') }}">
                        <div class="invalid-feedback"></div>
                    </div>
                    <div class="form-group">
                        <label for="">{{ __('lipton::user.modal.password_confirmation') }}</label>
                        <input name="password_confirmation" type="password" class="form-control" placeholder="{{ __('lipton::user.modal.password_tip') }}">
                        <div class="invalid-feedback"></div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('lipton::user.modal.close') }}</button>
                <button type="button" class="btn btn-primary">{{ __('lipton::user.modal.submit') }}</button>
            </div>
        </div>
    </div>
</div>

@push('scripts')
<script src="{{ Lipton::assets('/js/validator.min.js') }}"></script>
<script src="{{ Lipton::assets('/js/validation.js') }}"></script>
<script>
    $('#profile-modal')
        .on('show.bs.modal', function(e) {
            var self = this;
            $('.overlay', '#profile-modal').removeClass('hidden').addClass('shown');
            $.get('{{ route('lipton.user.showAuth') }}')
                .done(function(resp) {
                    $('input[name="email"]', self).val(resp.data.email);
                    $('input[name="name"]', self).val(resp.data.name);
                    $('.btn-primary', self).data(resp.data);
                })
                .always(function() {
                    $('.overlay', '#profile-modal').removeClass('shown').addClass('hidden');
                });
        })
        .on('hidden.bs.modal', function() {
            $('input.form-control', this).val('');
        })
        .on('click', '.btn-primary', function () {
            var $form = $(this).parents('.modal-content').find('form');
            var userId = $(this).data('id');
            $form.find('.is-invalid').removeClass('is-invalid');
            $form.validate({
                name: {
                    isLength: {
                        args: [{ min: 1 }],
                        message: "{{ __('lipton::user.feedback.invalid_name') }}"
                    }
                },
                password_confirmation: {
                    confirmed: {
                        // jQuery selector
                        args: ['#profile-modal input[name="password"]'],
                        message: "{{ __('lipton::user.feedback.invalid_password_confirmed') }}"
                    }
                }
            }, function() {
                $('.overlay', '#profile-modal').removeClass('hidden').addClass('shown');
                var data = {
                    name: $('input[name="name"]', $form).val()
                };
                var password = $('input[name="password"]').val();
                var password_confirmation = $('input[name="password_confirmation"]').val();
                if (/\w+/.test(password) && /\w+/.test(password_confirmation)) {
                    data.password = password;
                    data.password_confirmation = password_confirmation;
                }

                $.ajax({
                    url: '{{ route('lipton.user.update', ':id') }}'.replace(':id', userId),
                    type: 'put',
                    data: JSON.stringify(data),
                    dataType: 'json',
                    headers: {
                        'Content-type': 'application/json'
                    }
                }).done(function(resp) {
                    if (resp.code > 0) {
                        Toast.fire({
                            type: 'warning',
                            title: '{{ __('lipton::user.message.update_invalid') }}'
                        });
                        return false;
                    }
                    Toast.fire({
                        type: 'success',
                        title: '{{ __('lipton::user.message.update_success') }}'
                    });
                    $('#profile-modal').modal('hide');
                }).always(function() {
                    $('.overlay', '#profile-modal').removeClass('shown').addClass('hidden');
                });
            }, function(ele, ruleName, message) {
                // Invalid
                $(ele).addClass('is-invalid').siblings('.invalid-feedback').html(message);
            });
        });
</script>
@endpush