@extends('lipton::layout')

@push('pre-styles')
<link rel="stylesheet" href="{{ Lipton::assets('/admin-lte/plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
<link rel="stylesheet" href="{{ Lipton::assets('/bootstrap4-toggle/bootstrap4-toggle.min.css') }}">
<style>
    #cms-crawler-rules-modal .tab-content,
    #cms-crawler-modal .tab-content {
        padding: .6rem;
        border-left: 1px solid #dee2e6;
        border-right: 1px solid #dee2e6;
        border-bottom: 1px solid #dee2e6;
    }
    #content-page td {
        padding: .4rem .2rem;
    }
    .test-response {
        max-height: 12rem;
        overflow-y: auto;
        margin-top: 1rem;
        border: 2px dashed rgba(0,0,0,.2);
        padding: .2rem;
        border-radius: .6rem;
        min-height: 6rem;
    }
    #log-modal img {
        width: 100%;
    }
    #log-modal .modal-body {
        overflow: scroll;
        height: 35rem;
    }
    .modal .hint {
        font-size: .2rem;
        color: #999;
    }
    .form-row.page-row {
        margin-bottom: 1rem;
    }
    .form-row.page-row > .form-group {
        margin-bottom: .2rem;
    }
</style>
@endpush

@section('page_toolbar')
<div class="toolbar">
    @can('create', 'cms_crawler')
    <button class="btn btn-primary" data-toggle="modal" data-target="#cms-crawler-modal">
        {{ __('lipton::cms.crawler.toolbar.create') }}
    </button>
    @endcan
    <button class="btn btn-info" data-toggle="modal" data-target="#log-modal">
        {{ __('lipton::cms.crawler.toolbar.log') }}
    </button>
</div>
@endsection

@section('page_breadcrumb')
    <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="{{ route(config('lipton.default_route')) }}">{{ __('lipton::common.nav.home') }}</a></li>
        <li class="breadcrumb-item active">{{ __('lipton::cms.nav.crawler.index') }}</li>
    </ol>
@endsection

@section('container')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <table id="datatable" class="table table-bordered table-striped">
                        <thead>
                            <th>{{ __('lipton::cms.crawler.table.title') }}</th>
                            {{-- <th>{{ __('lipton::cms.crawler.table.domain') }}</th> --}}
                            <th>{{ __('lipton::cms.crawler.table.category') }}</th>
                            <th>{{ __('lipton::cms.crawler.table.schedule') }}</th>
                            <th>{{ __('lipton::cms.crawler.table.collect') }}</th>
                            <th>{{ __('lipton::cms.crawler.table.trigger') }}</th>
                            <th>{{ __('lipton::cms.crawler.table.operation') }}</th>
                            <th>{{ __('lipton::cms.crawler.table.action') }}</th>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="cms-crawler-modal" tabindex="-1" role="dialog" data-backdrop="static">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="overlay hidden">
                    <i class="fas fa-2x fa-sync-alt"></i>
                </div>
                <div class="modal-body">
                    <ul class="nav nav-tabs">
                        <li class="nav-item">
                            <a href="#general" data-toggle="pill" class="nav-link active">
                                {{ __('lipton::cms.crawler.modal.tab_general') }}
                            </a>
                        </li>
                        {{-- <li class="nav-item"><a href="#" data-toggle="pill" class="nav-link"></a></li> --}}
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade active show" id="general">
                            <form>
                                <div class="form-group">
                                    <label for="">{{ __('lipton::cms.crawler.modal.form.title') }}<span style="color: red">*</span></label>
                                    <input name="title" type="text" class="form-control" placeholder="{{ __('lipton::cms.crawler.modal.form.title_placeholder') }}">
                                    <div class="invalid-feedback"></div>
                                </div>
                                <div class="form-group">
                                    <label for="">{{ __('lipton::cms.crawler.modal.form.domain') }}</label>
                                    <input name="domain" type="text" class="form-control" placeholder="{{ __('lipton::cms.crawler.modal.form.domain_placeholder') }}">
                                    <div class="invalid-feedback"></div>
                                </div>
                                <div class="form-group">
                                    <label for="">{{ __('lipton::cms.crawler.modal.form.category') }}</label>
                                    <select name="cms_category_id" class="form-control">
                                        <option value="">{{ __('lipton::cms.crawler.modal.form.no_category') }}</option>
                                        @foreach($categories as $category)
                                            <option value="{{ $category->id }}">
                                                {{ $category->title }}
                                                {{ isset($category->parent) ? "({$category->parent->title})" : '' }}
                                            </option>
                                        @endforeach
                                    </select>
                                    <div class="invalid-feedback"></div>
                                </div>
                                <div class="form-group">
                                    <label for="">{{ __('lipton::cms.crawler.modal.form.image_plan') }}</label>
                                    <div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="image_solution" id="image_solution1" value="0" checked>
                                            <label class="form-check-label" for="image_solution1">
                                                {{ __('lipton::cms.crawler.modal.form.image_plan_pass') }}
                                            </label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="image_solution" id="image_solution2" value="1">
                                            <label class="form-check-label" for="image_solution2">
                                                {{ __('lipton::cms.crawler.modal.form.image_plan_download') }}
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="">{{ __('lipton::cms.crawler.modal.form.charset') }}</label>
                                    <div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="charset" id="charset1" value="utf8" checked>
                                            <label class="form-check-label" for="charset1">UTF8</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="charset" id="charset2" value="gb2312">
                                            <label class="form-check-label" for="charset2">GB2312</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="">{{ __('lipton::cms.crawler.modal.form.translate') }}</label>
                                    <div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="translate" id="translate1" value="0" checked>
                                            <label class="form-check-label" for="translate1">
                                                {{ __('lipton::cms.crawler.modal.form.translate_pass') }}
                                            </label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="translate" id="translate2" value="1">
                                            <label class="form-check-label" for="translate2">
                                                {{ __('lipton::cms.crawler.modal.form.translate_t2s') }}
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        {{-- <div class="tab-pane fade" id=""></div> --}}
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('lipton::cms.crawler.modal.close') }}</button>
                    <button type="button" class="btn btn-primary">{{ __('lipton::cms.crawler.modal.submit') }}</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="cms-crawler-schedule-modal" tabindex="-1" role="dialog" data-backdrop="static">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="overlay hidden">
                    <i class="fas fa-2x fa-sync-alt"></i>
                </div>
                <div class="modal-header">
                    <h5 class="modal-title create">{{ __('lipton::cms.crawler.modal.schedule_title') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <label for="">{{ __('lipton::cms.crawler.modal.schedule.method') }}</label>
                            <select name="method" class="form-control">
                                @foreach($frequency as $code => $title)
                                    <option value="{{ $code }}">{{ $title }}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback"></div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('lipton::cms.crawler.modal.close') }}</button>
                    <button type="button" class="btn btn-primary">{{ __('lipton::cms.crawler.modal.submit') }}</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="cms-crawler-rules-modal" tabindex="-1" role="dialog" data-backdrop="static">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="overlay hidden">
                    <i class="fas fa-2x fa-sync-alt"></i>
                </div>
                <div class="modal-body">
                    <ul class="nav nav-tabs">
                        <li class="nav-item">
                            <a href="#list-page" data-toggle="pill" class="nav-link active">
                                {{ __('lipton::cms.crawler.rules.tab_list') }}
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#content-page" data-toggle="pill" class="nav-link">
                                {{ __('lipton::cms.crawler.rules.tab_content') }}
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade active show" id="list-page">
                            <div class="form-group">
                                <label for="">
                                    {{ __('lipton::cms.crawler.rules.list_start_url') }}
                                    <span class="hint">({{ __('lipton::cms.crawler.rules.url_number_hint') }})</span>
                                </label>
                                <textarea name="list_start_url" rows="5" class="form-control" placeholder="{{ __('lipton::cms.crawler.rules.list_start_url_hint') }}"></textarea>
                                <div class="invalid-feedback"></div>
                            </div>
                            <div class="form-row page-row">
                                <div class="form-group col-4">
                                    <label for="">{{ __('lipton::cms.crawler.rules.start_number') }}</label>
                                    <input type="number" name="start_number" value="0" class="form-control" placeholder="">
                                    <div class="invalid-feedback"></div>
                                </div>
                                <div class="form-group col-4">
                                    <label for="">{{ __('lipton::cms.crawler.rules.end_number') }}</label>
                                    <input type="number" name="end_number" value="0" class="form-control" placeholder="">
                                    <div class="invalid-feedback"></div>
                                </div>
                                <div class="form-group col-4">
                                    <label for="">{{ __('lipton::cms.crawler.rules.limit') }}</label>
                                    <input type="number" name="limit" class="form-control" placeholder="10">
                                    <div class="invalid-feedback"></div>
                                </div>
                                <div class="col-12">
                                    <div class="hint">{{ __('lipton::cms.crawler.rules.number_hint_1') }}</div>
                                    <div class="hint">{{ __('lipton::cms.crawler.rules.number_hint_2') }}</div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group">
                                    <label style="margin-right: .55rem">{{ __('lipton::cms.crawler.rules.when_number_end') }}</label>
                                    <div class="form-check form-check-inline">
                                        <input
                                            class="form-check-input"
                                            type="radio"
                                            name="followup_action"
                                            id="followup_action_1"
                                            value="{{ \KevinKao\Lipton\Constants::CMS_CRAWLER_FOLLOWACTION_1 }}"
                                            checked="checked">
                                        <label class="form-check-label" for="followup_action_1">
                                            {{ __('lipton::cms.crawler.rules.keep_crawl_last_number') }}
                                        </label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input
                                            class="form-check-input"
                                            type="radio"
                                            name="followup_action"
                                            id="followup_action_2"
                                            value="{{ \KevinKao\Lipton\Constants::CMS_CRAWLER_FOLLOWACTION_2 }}">
                                        <label class="form-check-label" for="followup_action_2">
                                            {{ __('lipton::cms.crawler.rules.stop_crawling') }}
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-6">
                                    <label for="">{{ __('lipton::cms.crawler.rules.list_range') }}</label>
                                    <input type="text" name="list_range" class="form-control" placeholder=".item-content li">
                                    <div class="invalid-feedback"></div>
                                </div>
                                <div class="form-group col-6">
                                    <label for="">{{ __('lipton::cms.crawler.rules.list_selector') }}<span class="hint">({{ __('lipton::cms.crawler.rules.list_selector_hint') }})</span></label>
                                    <input type="text" name="list_selector" class="form-control" placeholder="h1 > a">
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>
                            <button type="button" id="test-list-rule" class="btn btn-info btn-sm test-btn" style="margin-top: .4rem" data-spinner-text="{{ __('lipton::cms.crawler.rules.crawling') }}">
                                {{ __('lipton::cms.crawler.rules.just_test') }}
                            </button>
                            <button type="button" data-target="#test-list-response" class="btn btn-info btn-sm clean-result-btn" style="margin-top: .4rem">{{ __('lipton::cms.crawler.rules.clean_test_content') }}</button>
                            <div id="test-list-response" class="test-response"></div>
                        </div>
                        <div class="tab-pane fade" id="content-page">
                            <table class="table"></table>
                            <div style="width: 100%">
                                <button class="w-100 btn btn-sm btn-outline-success create">
                                    {{ __('lipton::cms.crawler.rules.add_column') }}
                                </button>
                            </div>

                            <div style="margin-top: 1.4rem;">
                                <div class="form-group">
                                    <label for="">{{ __('lipton::cms.crawler.rules.test_content_url') }}<span style="font-size: .2rem;color: #999">({{ __('lipton::cms.crawler.rules.test_content_url_hint') }})</span></label>
                                    <input type="text" name="test_content_url" class="form-control" placeholder="http://example.com/xxxx/2323.html">
                                    <div class="invalid-feedback"></div>
                                </div>
                                <button type="button" id="test-content-rule" class="btn btn-info btn-sm test-btn" data-spinner-text="{{ __('lipton::cms.crawler.rules.crawling') }}">{{ __('lipton::cms.crawler.rules.just_test') }}</button>
                                <button type="button" data-target="#test-content-response" class="btn btn-info btn-sm clean-result-btn">{{ __('lipton::cms.crawler.rules.clean_test_content') }}</button>
                                <div id="test-content-response" class="test-response"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('lipton::cms.crawler.modal.close') }}</button>
                    <button type="button" class="btn btn-primary">{{ __('lipton::cms.crawler.modal.submit') }}</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="log-modal" tabindex="-1" role="dialog" aria-labelledby="logModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{ __('lipton::cms.crawler.modal.log_title') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script src="{{ Lipton::assets('/admin-lte/plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ Lipton::assets('/admin-lte/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
<script src="{{ Lipton::assets('/js/validator.min.js') }}"></script>
<script src="{{ Lipton::assets('/js/validation.js') }}"></script>
<script src="{{ Lipton::assets('/bootstrap4-toggle/bootstrap4-toggle.min.js') }}"></script>
<script src="{{ Lipton::assets('/js/spinner.js') }}"></script>
@datatable_locale()
<script>
    $('#log-modal').on('show.bs.modal', function() {
        $("#log-modal .modal-body").empty().html('读取中...');
        $.get('{{ route('lipton.cms.crawler.log') }}').done(function(resp) {
            $("#log-modal .modal-body").empty();
            for (var i in resp) {
                var line = resp[i];
                if (line) {
                    $("#log-modal .modal-body").append('<div>'+line+'</div>');
                }
            }
        });
    });
    $('#datatable').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "info": true,
        "autoWidth": false,
        "serverSide": true,
        "processing": true,
        "stateSave": true,
        "ordering": false,
        "ajax": {
            url: '{{ route('lipton.cms.crawler.datatable') }}',
            type: 'post'
        },
        "createdRow": function(row, data, dataIndex) {
            $(row).data(data);
        },
        "columns": [
            { "data": "title", "width": "10%" },
            // { "data": "domain", "width": "10%" },
            { "data": "category_title", "width": "10%", "class": "text-center" },
            { "data": "id", "width": "10%", "class": "text-center", render: function(data, type, row) {
                return '<button class="btn btn-success btn-sm" data-toggle="modal" data-target="#cms-crawler-schedule-modal" data-id="'+row.id+'" '+ (row.canEdit ? '' : 'disabled="disabled"' )+'>'+row.frequency+'</button>';
            } },
            { "data": "id", "width": "5%", "class": "text-center", render: function(data, type, row) {
                return '<button class="btn btn-info btn-sm" data-toggle="modal" data-target="#cms-crawler-rules-modal" data-id="'+data+'" '+ (row.canEdit ? '' : 'disabled="disabled"' )+'>{{ __('lipton::cms.crawler.table.rules') }}</button>';
            } },
            { "data": "id", "width": "5%", "class": "text-center", render: function(data, type, row) {
                return '<button class="btn btn-warning btn-sm trigger" data-spinner-text="loading" data-id="'+data+'" '+ (row.canEdit ? '' : 'disabled="disabled"' )+'>{{ __('lipton::cms.crawler.table.trigger') }}</button>';
            } },
            { "data": "operation", "width": "5%", "class": "text-center", render: function(data, type, row) {
                var $input = $('<input/>', {type: 'checkbox', name: 'visible', 'data-idx': row.id}).addClass('operation-toggle');
                if (!row.canEdit) {
                    $input.attr('disabled', true);
                }
                var isOperating = data == 1;
                if (isOperating) {
                    $input.attr('checked', true);
                }
                return $input.prop('outerHTML');
            } },
            { "data": "action", "width": "10%" }
        ],
        "drawCallback": function() {
            $('.operation-toggle', this).bootstrapToggle({ size: 'sm' });
        }
    }).on('click', '.delete', function() {
        var id = $(this).data('id');
        Swal.fire({
            title: '{{ __('lipton::cms.crawler.message.delete_confirmed') }}',
            text: "{{ __('lipton::cms.crawler.message.delete_tip') }}",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: '{{ __('lipton::cms.crawler.message.confirm_text') }}',
            cancelButtonText: '{{ __('lipton::cms.crawler.message.cancel_text') }}'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: '{{ route('lipton.cms.crawler.destroy', ':id') }}'.replace(':id', id),
                    type: 'delete'
                }).done(function(resp) {
                    if (resp.code > 0) {
                        Toast.fire({ type: 'error', title: resp.message });
                        return;
                    }
                    $('#datatable').DataTable().ajax.reload(null, false);
                    Toast.fire({ type: 'success', title: resp.message });
                });
            }
        });
    }).on('change', '.operation-toggle', function() {
        var self = this;
        var id = $(this).data('idx');

        $(self).prop('disabled', true);
        $.ajax({
            url: '{{ route('lipton.cms.crawler.operation', ':id') }}'.replace(':id', id),
            type: 'put',
            dataType: 'json',
            data: JSON.stringify({ operation: $(this).prop('checked') }),
            headers: { 'Content-type': 'application/json' }
        }).done(function(resp) {
            if (resp.code > 0) {
                Toast.fire({ type: 'error', title: resp.message });
                return false;
            }
            Toast.fire({ type: 'success', title: resp.message });
        }).always(function() {
            $(self).prop('disabled', false);
        });
    }).on('click', '.trigger', function() {
        var self = this;
        var id = $(this).data('id');
        Swal.fire({
            title: '{{ __('lipton::cms.crawler.message.trigger_confirmed') }}',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: '{{ __('lipton::cms.crawler.message.confirm_text') }}',
            cancelButtonText: '{{ __('lipton::cms.crawler.message.cancel_text') }}'
        }).then((result) => {
            if (result.value) {
                $(self).spinner('block');
                $.ajax({
                    url: '{{ route('lipton.cms.crawler.trigger', ':id') }}'.replace(':id', id),
                    type: 'post'
                }).done(function(resp) {
                    if (resp.code > 0) {
                        Toast.fire({ type: 'error', title: resp.message });
                        return;
                    }
                    Toast.fire({ type: 'success', title: resp.message });
                }).always(function() {
                    $(self).spinner('unblock');
                });
            }
        })
    });

    $('#cms-crawler-modal')
        .on('hidden.bs.modal', function(e) {
            $('input[type=text]', this).val('');
            $('select[name=cms_category_id]', this).val(
                $('select[name=cms_category_id] option:first', this).val()
            );
            $('[name="image_solution"]:first').prop('checked', true);
            $('[name="charset"]:first').prop('checked', true);
            $('[name="translate"]:first').prop('checked', true);
        })
        .on('show.bs.modal', function(e) {
            var id = $(e.relatedTarget).data('id');
            if (id) {
                $('button.btn-primary', this).data({
                    'type': 'put',
                    'url': '{{ route('lipton.cms.crawler.update', ':id') }}'.replace(':id', id),
                });
                var data = $(e.relatedTarget).parents('tr').data();
                $('input[name=title]', this).val(data.title);
                $('input[name=domain]', this).val(data.domain);
                $('select[name=cms_category_id]', this).val(data.cms_category_id);
                $('input[name=image_solution][value='+data.image_solution+']', this).prop('checked', true);
                $('h5.update', this).removeClass('d-none');
                $('h5.create', this).addClass('d-none');
                $('input[name=charset][value='+data.charset+']', this).prop('checked', true);
                $('input[name=translate][value='+data.translate+']', this).prop('checked', true);
            } else {
                $('h5.update', this).addClass('d-none');
                $('h5.create', this).removeClass('d-none');
                $('button.btn-primary', this).data({
                    'type': 'post',
                    'url': '{{ route('lipton.cms.crawler.store') }}',
                });
            }
        })
        .on('click', '.btn-primary', function() {
            var self = this;
            var $form = $(this).parents('.modal-content').find('form');
            $form.find('.is-invalid').removeClass('is-invalid');
            $form.validate({
                title: {
                    required: {
                        message: "{{ __('lipton::cms.crawler.feedback.is_required', ['field' => __('lipton::cms.crawler.modal.title')]) }}"
                    }
                }
            }, function() {
                var form = this;
                var data = {
                    title: $('input[name=title]', form).val(),
                    domain: $('input[name=domain]', form).val(),
                    cms_category_id: $('select[name=cms_category_id]', form).val(),
                    image_solution: $('input[name=image_solution]:checked', form).val(),
                    charset: $('input[name=charset]:checked', form).val(),
                    translate: $('input[name=translate]:checked', form).val()
                };
                $('.overlay', '#cms-crawler-modal').removeClass('hidden').addClass('shown');
                $.ajax({
                    url: $(self).data('url'),
                    type: $(self).data('type'),
                    dataType: 'json',
                    data: JSON.stringify(data),
                    headers: {
                        'Content-type': 'application/json'
                    }
                }).done(function(resp) {
                    if (resp.code > 0) {
                        Toast.fire({ type: 'error', title: resp.message });
                        return false;
                    }
                    $('#datatable').DataTable().ajax.reload(null, false);
                    Toast.fire({ type: 'success', title: resp.message });
                    $('#cms-crawler-modal').modal('hide');
                }).always(function() {
                    $('.overlay', '#cms-crawler-modal').removeClass('shown').addClass('hidden');
                });

            }, function(ele, ruleName, message) {
                $(ele).addClass('is-invalid').siblings('.invalid-feedback').html(message);
            });
        });

        $('#cms-crawler-schedule-modal')
            .on('hidden.bs.modal', function(e) {
                $('select[name=method]', this).val(
                    $('select[name=method] option:first', this).val()
                );
            })
            .on('show.bs.modal', function(e) {
                var self = this;
                var id = $(e.relatedTarget).data('id');
                $('button.btn-primary', self).data('id', id);
                $('.overlay', self).removeClass('hidden').addClass('shown');
                var url = '{{ route('lipton.cms.crawler.schedule.show', ':id') }}'.replace(':id', id);
                $.get(url).done(function(resp) {
                    if (resp.code > 0) {
                        Toast.fire({ type: 'error', title: resp.message });
                        return false;
                    }
                    $('select[name=method]', self).val(resp.data.method);
                }).always(function() {
                    $('.overlay', self).removeClass('shown').addClass('hidden');
                });
            })
            .on('click', 'button.btn-primary', function() {
                var id = $(this).data('id');
                var $form = $(this).parents('.modal-content').find('form');
                var data = {
                    method: $('select[name=method]', $form).val()
                };
                $('.overlay', '#cms-crawler-schedule-modal').removeClass('hidden').addClass('shown');
                $.ajax({
                    url: '{{ route('lipton.cms.crawler.schedule.update', ':id') }}'.replace(':id', id),
                    type: 'put',
                    dataType: 'json',
                    data: JSON.stringify(data),
                    headers: {
                        'Content-type': 'application/json'
                    }
                }).done(function(resp) {
                    if (resp.code > 0) {
                        Toast.fire({ type: 'error', title: resp.message });
                        return false;
                    }
                    Toast.fire({ type: 'success', title: resp.message });
                    $('#datatable').DataTable().ajax.reload(null, false);
                    $('#cms-crawler-schedule-modal').modal('hide');
                }).always(function() {
                    $('.overlay', '#cms-crawler-schedule-modal').removeClass('shown').addClass('hidden');
                });
            });

    $('#cms-crawler-rules-modal')
        .on('hidden.bs.modal', function(e) {
            var self = this;
            $('[name=list_start_url]', self).val('');
            $('[name=limit]', self).val('');
            $('[name=list_range], [name=list_selector]', self).val('');
            $('table tr', self).remove();
            $('[name=test_content_url]', self).val('');
            $('.clean-result-btn').trigger('click');
        })
        .on('show.bs.modal', function(e) {
            var self = this;
            var id = $(e.relatedTarget).data('id');
            $('button.btn-primary', self).data('id', id);
            $('.test-btn', self).data('id', id);

            $('.overlay', self).removeClass('hidden').addClass('shown');
            $.get('{{ route('lipton.cms.crawler.rules.index', ':id') }}'.replace(':id', id))
                .done(function(resp) {
                    $('[name=list_start_url]', self).val(resp.data.list.start_url);
                    $('[name=limit]', self).val(resp.data.list.limit);
                    $('[name=list_selector]', self).val(resp.data.list.selector);
                    $('[name=list_range]', self).val(resp.data.list.range);
                    $('[name=start_number]', self).val(resp.data.list.start_number);
                    $('[name=end_number]', self).val(resp.data.list.end_number);
                    $('[name=followup_action][value='+resp.data.list.followup_action+']', self).prop('checked', true);

                    $.each(resp.data.content, function(_, row) {
                        var $tr = $('<tr/>').appendTo('#content-page > table');
                        $('<td/>')
                            .css('width', '20%')
                            .html(
                                $('<select/>', { name: 'column' }).addClass('form-control form-control-sm')
                                    .append('<option value="title">標題</option>')
                                    .append('<option value="content">內容</option>')
                                    .append('<option value="cover">封面</option>')
                                    .val(row.column)
                            )
                            .appendTo($tr);
                        $('<td/>')
                            .html($('<input/>', { name: 'selector', type: 'text', placeholder: 'h1' }).addClass('form-control form-control-sm').val(row.selector))
                            .appendTo($tr);
                        $('<td/>')
                            .html($('<input/>', { name: 'type', type: 'text', placeholder: 'text|html' }).addClass('form-control form-control-sm').val(row.type))
                            .appendTo($tr);
                        $('<td/>')
                            .html($('<input/>', { name: 'filter', type: 'text', placeholder: '-.editor' }).addClass('form-control form-control-sm').val(row.filter))
                            .appendTo($tr);
                        $('<td/>')
                            .html($('<button/>').addClass('btn btn-sm btn-outline-danger trash').html($('<i/>').addClass('fa fa-trash')))
                            .appendTo($tr);
                    });
                })
                .always(function() {
                    $('.overlay', self).removeClass('shown').addClass('hidden');
                });
        })
        .on('click', '.btn-primary', function() {
            var id = $(this).data('id');
            var $modal = $('#cms-crawler-rules-modal');
            var data = {
                list: {
                    start_url: $('textarea[name=list_start_url]', $modal).val(),
                    limit: $('input[name=limit]', $modal).val(),
                    range: $('input[name=list_range]', $modal).val(),
                    selector: $('input[name=list_selector]', $modal).val(),
                    start_number: $('input[name=start_number]', $modal).val(),
                    end_number: $('input[name=end_number]', $modal).val(),
                    followup_action: $('input[name=followup_action]:checked', $modal).val()
                },
                content: []
            };

            $('table tr', $modal).each(function() {
                var tr = this;
                data.content.push({
                    column: $('select[name="column"]', tr).val(),
                    selector: $('input[name="selector"]', tr).val(),
                    type: $('input[name="type"]', tr).val(),
                    filter: $('input[name="filter"]', tr).val()
                });
            });

            $('.overlay', '#cms-crawler-rules-modal').removeClass('hidden').addClass('shown');
            $.ajax({
                url: '{{ route('lipton.cms.crawler.rules.update', ':id') }}'.replace(':id', id),
                type: 'put',
                data: JSON.stringify(data),
                contentType: 'application/json'
            }).done(function(resp) {
                if (resp.code > 0) {
                    Toast.fire({ type: 'error', title: resp.message });
                    return false;
                }
                Toast.fire({ type: 'success', title: resp.message });
                $('#cms-crawler-rules-modal').modal('hide');
            }).always(function() {
                $('.overlay', '#cms-crawler-rules-modal').removeClass('shown').addClass('hidden');
            });
        })
        .on('click', '.create', function() {
            var $tr = $('<tr/>').appendTo('#content-page > table');
            $('<td/>')
                .css('width', '20%')
                .html(
                    $('<select/>', { name: 'column' }).addClass('form-control form-control-sm')
                        .append('<option value="title">標題</option>')
                        .append('<option value="content">內容</option>')
                        .append('<option value="cover">封面</option>')
                )
                .appendTo($tr);
            $('<td/>')
                .html($('<input/>', { name: 'selector', type: 'text', placeholder: 'h1' }).addClass('form-control form-control-sm'))
                .appendTo($tr);
            $('<td/>')
                .html($('<input/>', { name: 'type', type: 'text', placeholder: 'text|html' }).addClass('form-control form-control-sm'))
                .appendTo($tr);
            $('<td/>')
                .html($('<input/>', { name: 'filter', type: 'text', placeholder: '-.editor' }).addClass('form-control form-control-sm'))
                .appendTo($tr);
            $('<td/>')
                .html($('<button/>').addClass('btn btn-sm btn-outline-danger trash').html($('<i/>').addClass('fa fa-trash')))
                .appendTo($tr);
        })
        .on('click', '.trash', function() {
            $(this).parents('tr').remove();
        });

    $('.clean-result-btn').on('click', function() {
        $($(this).data('target')).empty();
    });

    $('#test-list-rule').on('click', function() {
        var self = this;
        var $modal = $('#cms-crawler-rules-modal');
        var data = {
            start_url: $('textarea[name=list_start_url]', $modal).val(),
            limit: $('input[name=limit]', $modal).val(),
            range: $('input[name=list_range]', $modal).val(),
            selector: $('input[name=list_selector]', $modal).val()
        }
        $(self).spinner('block');
        $.ajax({
            url: '{{ route('lipton.cms.crawler.list.test') }}',
            type: 'post',
            data: JSON.stringify(data),
            contentType: 'application/json'
        }).done(function(resp) {
            var $response = $('#test-list-response').empty();
            $.each(resp.data, function(_, row) {
                $response.append('<div>'+row.link+'</div>')
            });
        }).always(function() {
            $(self).spinner('unblock');
        });
    });

    $('#test-content-rule').on('click', function() {
        var self = this;
        var data = {
            crawler_id: $(self).data('id'),
            url: $('input[name=test_content_url]', '#cms-crawler-rules-modal').val(),
            rules: []
        }

        $('table tr', '#cms-crawler-rules-modal').each(function(tr) {
            var tr = this;
            data.rules.push({
                column: $('select[name="column"]', tr).val(),
                selector: $('input[name="selector"]', tr).val(),
                type: $('input[name="type"]', tr).val(),
                filter: $('input[name="filter"]', tr).val()
            });
        });

        $(self).spinner('block');
        $.ajax({
            url: '{{ route('lipton.cms.crawler.content.test') }}',
            type: 'post',
            data: JSON.stringify(data),
            contentType: 'application/json'
        }).done(function(resp) {
            var $response = $('#test-content-response').empty();
            $.each(resp.data, function(column, value) {
                $response.append('<div>['+column+'] => '+value+'</div>');
            });
        }).always(function() {
            $(self).spinner('unblock');
        });
    });

    $('#log-modal').on('shown.bs.modal', function() {
        var $body = $('.modal-body', this);
        $body.scrollTop($body[0].scrollHeight);
    });
</script>
@endpush
