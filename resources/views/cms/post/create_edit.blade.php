@extends('lipton::layout')

@push('styles')
<style>
    .toolbar.fixed {
        position: fixed;
        top: .4rem;
        z-index: 55;
    }
    #meta-table th,
    #meta-table td {
        padding: .25rem;
    }
</style>
@endpush

@section('page_toolbar')
<div class="toolbar">
    <a href="{{ route('lipton.cms.post.index') }}" class="btn btn-info">{{ __('lipton::cms.post.toolbar.back') }}</a>
    <button id="submit" class="btn btn-primary animsition-link">{{ __('lipton::cms.post.toolbar.submit') }}</button>
</div>
@endsection

@section('page_breadcrumb')
    <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="{{ route(config('lipton.default_route')) }}">{{ __('lipton::common.nav.home') }}</a></li>
        <li class="breadcrumb-item"><a href="{{ route('lipton.cms.post.index') }}">{{ __('lipton::cms.nav.post.index') }}</a></li>
        @if (isset($post))
        <li class="breadcrumb-item active">{{ __('lipton::cms.nav.post.edit') }}</li>
        @else
        <li class="breadcrumb-item active">{{ __('lipton::cms.nav.post.create') }}</li>
        @endif
    </ol>
@endsection

@section('container')
<form method="POST" action="{{ $actionUrl }}" enctype="multipart/form-data">
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    {{ csrf_field() }}
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">
                            {{ __('lipton::cms.post.form.title') }}
                            <span class="text-danger">*</span>
                        </label>
                        <div class="col-sm-10">
                            <input
                                type="text"
                                name="title"
                                class="form-control @error('title') is-invalid @enderror"
                                value="{{ isset($post) ? $post->title : old('title') }}">
                            @error('title')<div class="invalid-feedback">{{ $message }}</div>@enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">{{ __('lipton::cms.post.form.category') }}</label>
                        <div class="col-sm-4">
                            <select name="cms_category_id" class="form-control">
                                <option value="">{{ __('lipton::cms.post.form.no_category') }}</option>
                                @foreach($categories as $category)
                                <option value="{{ $category->id }}"
                                    {{ isset($post) && $post->cms_category_id === $category->id ? 'selected' : '' }}>
                                    {{ $category->title }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">{{ __('lipton::cms.post.form.slug') }}</label>
                        <div class="col-sm-4">
                            <input
                                type="text"
                                name="slug"
                                class="form-control @error('slug') is-invalid @enderror"
                                placeholder="example-slug-name"
                                value="{{ $post->slug ?? old('slug') }}">

                            @error('slug')<div class="invalid-feedback">{{ $message }}</div>@enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">{{ __('lipton::cms.post.form.order') }}</label>
                        <div class="col-sm-2">
                            <input
                                type="text"
                                name="order"
                                class="form-control @error('order') is-invalid @enderror"
                                placeholder="50"
                                value="{{ $post->order ?? old('order') }}">

                            @error('order')<div class="invalid-feedback">{{ $message }}</div>@enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">{{ __('lipton::cms.post.form.external_link') }}</label>
                        <div class="col-sm-10">
                            <textarea
                                type="text"
                                name="external_link"
                                class="form-control @error('external_link') is-invalid @enderror"
                                placeholder="https://example.com"
                                >{{ $post->external_link ?? old('external_link') }}</textarea>
                            @error('external_link')<div class="invalid-feedback">{{ $message }}</div>@enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">{{ __('lipton::cms.post.form.score') }}</label>
                        <div class="col-sm-2">
                            <input
                                type="text"
                                name="score"
                                class="form-control @error('score') is-invalid @enderror"
                                placeholder="500"
                                value="{{ $post->score ?? old('score') }}">
                            @error('score')<div class="invalid-feedback">{{ $message }}</div>@enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">{{ __('lipton::cms.post.form.cover') }}</label>
                        <div class="col-sm-10">
                            <input type="file" name="cover" class="form-control-file" accept="image/*">
                            @if (isset($post) && !empty($post->cover_id))
                                <img src="{{ $post->cover->path }}" alt="" style="max-width: 100%;max-height: 20rem">
                            @endif
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label>{{ __('lipton::cms.post.form.content') }}</label>
                            <textarea name="content" class="tinymce-editor">
                                {{ $post->content ?? '' }}
                            </textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">
                        {{ __('lipton::cms.post.form.meta_title') }}
                    </h3>
                </div>
                <div class="card-body">
                    <table class="table" id="meta-table">
                        <thead>
                            <tr>
                                <th width="40%">{{ __('lipton::cms.post.form.meta_key') }}</th>
                                <th width="40%">{{ __('lipton::cms.post.form.meta_value') }}</th>
                                <th width="20%">
                                    <button id="make-meta" class="btn btn-success btn-sm">
                                        <i class="fas fa-plus"></i>
                                    </button>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (isset($post))
                                @foreach($post->meta as $meta)
                                    <tr>
                                        <td><input type="text" class="form-control form-control-sm" value="{{ $meta->k }}"></td>
                                        <td><input type="text" class="form-control form-control-sm" value="{{ $meta->v }}"></td>
                                        <td>
                                            <button class="btn btn-sm btn-danger trash">
                                                <i class="fas fa-trash"></i>
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection

@push('scripts')
<script src="{{ Lipton::assets('js/tinymce/tinymce-4.9.2.min.js') }}"></script>
@if (App::isLocale('zh_tw'))
<script src="{{ Lipton::assets('js/tinymce/lang/zh_tw.js') }}"></script>
@elseif(App::isLocale('zh_cn'))
<script src="{{ Lipton::assets('js/tinymce/lang/zh_cn.js') }}"></script>
@endif
<script>
    $(function() {
        @if (session('success'))
            Toast.fire({
                type: 'success',
                title: '{{ session('success') }}'
            });
        @endif
        @if ($errors->any())
            Toast.fire({
                type: 'error',
                title: '{{ $errors->first() }}'
            });
        @endif
    });

    $(window).on('scroll', function(e) {
        var $toolHeader = $('.content-header');
        var $toolbar = $('.toolbar');
        var toolbarOffset = $toolHeader.offset();
        if (window.pageYOffset > toolbarOffset.top && !$toolbar.hasClass('fixed')) {
            $toolbar.addClass('fixed');
        }
        if (window.pageYOffset <= toolbarOffset.top && $toolbar.hasClass('fixed')) {
            $toolbar.removeClass('fixed');
        }
    });
    tinymce.init({
        selector:'.tinymce-editor',
        plugins: 'table link preview textcolor colorpicker image lists advlist fullpage fullscreen pagebreak nonbreaking',
        toolbar: "undo redo | fullscreen preview | image | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link | table | pagebreak | forecolor backcolor4 | fontsizeselect",
        height: '400',
        relative_urls : false,
        convert_urls : false,
        file_picker_types: 'image',
        setup: function (editor) {
            editor.on('change', function () {
                tinymce.triggerSave();
            });
        },
        images_upload_handler: function (blobInfo, success, failure) {
            var xhr, formData;
            xhr = new XMLHttpRequest();
            xhr.withCredentials = false;
            xhr.open('POST', '{{ route('lipton.cms.post.editorUpload') }}');
            var token = $('meta[name="csrf-token"]').attr('content');
            xhr.setRequestHeader("X-CSRF-Token", token);
            xhr.onload = function () {
                var json;
                if (xhr.status != 200) {
                    failure('HTTP Error: ' + xhr.status);
                    return;
                }
                json = JSON.parse(xhr.responseText);
                if (json.code > 0) {
                    return failure(json.message);
                }

                if (!json || typeof json.src != 'string') {
                    failure('Invalid JSON: ' + xhr.responseText);
                    return;
                }

                success(json.src);
            };
            formData = new FormData();
            formData.append('file', blobInfo.blob(), blobInfo.filename());
            xhr.send(formData);
        }
    });

    $('#submit').on('click', function(e) {
        e.preventDefault();
        $('#meta-table tbody > tr').each(function(i, ele) {
            $('td:eq(0) > input', ele).attr('name', 'meta['+i+'][key]');
            $('td:eq(1) > input', ele).attr('name', 'meta['+i+'][value]');
        });
        $('form').submit();
    });

    $('#make-meta').on('click', function(e) {
        e.preventDefault();
        var $tr = $('<tr/>').appendTo($('#meta-table'));
        $('<td/>')
            .html($('<input/>').addClass('form-control form-control-sm'))
            .appendTo($tr);
        $('<td/>')
            .html($('<input/>').addClass('form-control form-control-sm'))
            .appendTo($tr);
        $('<td/>')
            .html($('<button/>').addClass('btn btn-sm btn-danger ').html('<i class="fas fa-trash" />'))
            .appendTo($tr);
    });

    $('#meta-table').on('click', '.trash', function(e) {
        e.preventDefault();
        $(this).parents('tr').remove();
    });
</script>
@endpush