@extends('lipton::layout')

@push('pre-styles')
<link rel="stylesheet" href="{{ Lipton::assets('/admin-lte/plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
{{-- <link rel="stylesheet" href="{{ Lipton::assets('/bootstrap4-toggle/bootstrap4-toggle.min.css') }}"> --}}
@endpush

@section('page_toolbar')
<div class="toolbar row mx-0">
    <div class="col-sm-3 px-0">
        <button class="btn btn-success" id="fresh-table">{{ __('lipton::cms.post.toolbar.fresh') }}</button>
        @can('create', 'cms_post')
            <a class="btn btn-primary" style="margin-left: .2rem" href="{{ route('lipton.cms.post.create') }}">
                {{ __('lipton::cms.post.toolbar.create') }}
            </a>
        @endcan
    </div>
    <div class="col-sm-9 pl-1 pr-0">
        <div class="form-inline">
            <select id="cms-category-id" class="form-control" style="max-width: 45%">
                <option value="all">{{ __('lipton::cms.post.toolbar.category_all') }}</option>
                @foreach($categories as $category)
                <option value="{{ $category->id }}">{{ $category->title }}</option>
                @endforeach
            </select>
            <input type="text" id="keyword" class="form-control ml-1" placeholder="{{ __('lipton::cms.post.toolbar.search_keyword') }}">
            <button id="search-btn" class="btn btn-secondary">{{ __('lipton::cms.post.toolbar.search_btn') }}</button>
        </div>
    </div>
</div>
@endsection

@section('page_breadcrumb')
    <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="{{ route(config('lipton.default_route')) }}">{{ __('lipton::common.nav.home') }}</a></li>
        <li class="breadcrumb-item active">{{ __('lipton::cms.nav.post.index') }}</li>
    </ol>
@endsection

@section('container')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <table id="datatable" class="table table-bordered table-striped">
                        <thead>
                            <th>{{ __('lipton::cms.post.table.id') }}</th>
                            <th>{{ __('lipton::cms.post.table.created_at') }}</th>
                            <th>{{ __('lipton::cms.post.table.category_title') }}</th>
                            <th>{{ __('lipton::cms.post.table.title') }}</th>
                            <th>{{ __('lipton::cms.post.table.action') }}</th>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script src="{{ Lipton::assets('/admin-lte/plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ Lipton::assets('/admin-lte/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
@datatable_locale()
<script>
    var $datatable = $('#datatable').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "info": true,
        "autoWidth": false,
        "serverSide": true,
        "processing": true,
        "stateSave": true,
        "searching": false,
        "order": [[ 1, "desc" ]],
        "createdRow": function(row, data, dataIndex) {
            $(row).data(data);
        },
        "ajax": {
            'url': "{{ route('lipton.cms.post.datatable') }}",
            type: 'post',
            'data': function (data) {
                data.search.cms_category_id = $('#cms-category-id').val();
                data.search.keyword = $('#keyword').val();
            }
        },
        "columns": [
            {"data": "id", "width": "10%"},
            {"data": "created_at", "width": "15%"},
            {"data": "category_title", "width": "15%", "orderable": false},
            {"data": "title", "width": "45%", "orderable": false},
            {"data": "action", "width": "15%", "orderable": false}
        ]
    }).on('click', '.delete', function() {
        var id = $(this).data('id');
        Swal.fire({
            title: '{{ __('lipton::cms.post.message.delete_confirmed') }}',
            text: "{{ __('lipton::cms.post.message.delete_tip') }}",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: '{{ __('lipton::cms.post.message.confirm_text') }}',
            cancelButtonText: '{{ __('lipton::cms.post.message.cancel_text') }}'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: '{{ route('lipton.cms.post.destroy', ':id') }}'.replace(':id', id),
                    type: 'delete'
                }).done(function(resp) {
                    var type = resp.code > 0 ? 'error' : 'success';
                    $('#datatable').DataTable().ajax.reload(null, false);
                    Toast.fire({ type: type, title: resp.message });
                });
            }
        });
    }).on('draw', function() {
        var $searchBtn = $('#search-btn');
        $searchBtn.attr('disabled', false);
    });

    $('#fresh-table').on('click', function() {
        $datatable.ajax.reload(null, false);
    });

    $('#search-btn').on('click', function() {
        var $self = $(this);
        $self.attr('disabled', true);
        $datatable.page('first').draw('page');
    });

</script>
@endpush