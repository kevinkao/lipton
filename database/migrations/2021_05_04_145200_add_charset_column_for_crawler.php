<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCharsetColumnForCrawler extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cms_crawlers', function (Blueprint $table) {
            $table->string('charset')->default('utf8')->after('operation');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cms_crawlers', function (Blueprint $table) {
            $table->dropColumn(['charset']);
        });
    }
}
