<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTranslateForCmsCrawlers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cms_crawlers', function (Blueprint $table) {
            $table->tinyInteger('translate')->default(0)->comment('0:不轉換, 1:轉簡體')->after('charset');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cms_crawlers', function (Blueprint $table) {
            $table->dropColumn(['translate']);
        });
    }
}
