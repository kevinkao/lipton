<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCrawlerListNumberColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cms_crawler_list_rule', function (Blueprint $table) {
            $table->integer('start_number')->default(0);
            $table->integer('end_number')->default(0);
            $table->integer('current_number')->default(-1);
            $table->tinyInteger('followup_action')->default(1)->comment('號碼迭代結束後續動作, 1: 只採集結束號碼, 2: 終止採集');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cms_crawler_list_rule', function (Blueprint $table) {
            $table->dropColumn(['start_number', 'end_number', 'current_number', 'followup_action']);
        });
    }
}
