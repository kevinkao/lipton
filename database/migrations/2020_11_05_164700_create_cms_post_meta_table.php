<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCmsPostMetaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_post_meta', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('cms_post_id');
            $table->string('k', 255);
            $table->string('v', 255)->nullable();

            $table->foreign('cms_post_id')
                ->references('id')->on('cms_posts')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_post_meta');
    }
}
