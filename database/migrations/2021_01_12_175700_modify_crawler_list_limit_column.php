<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModifyCrawlerListLimitColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cms_crawler_list_rule', function (Blueprint $table) {
            $table->integer('limit')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cms_crawler_list_rule', function (Blueprint $table) {
            $table->tinyInteger('limit')->change();
        });
    }
}
