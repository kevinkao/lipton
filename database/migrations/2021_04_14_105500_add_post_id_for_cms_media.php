<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPostIdForCmsMedia extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cms_media', function (Blueprint $table) {
            $table->unsignedInteger('post_id')->nullable()->after('title');
            $table->foreign('post_id')
                ->references('id')->on('cms_posts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cms_media', function (Blueprint $table) {
            $table->dropForeign('cms_media_post_id_foreign');
            $table->dropColumn(['post_id']);
        });
    }
}
