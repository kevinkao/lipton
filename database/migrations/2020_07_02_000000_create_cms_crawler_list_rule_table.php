<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCmsCrawlerListRuleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_crawler_list_rule', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('crawler_id');
            $table->text('start_url')->nullable();
            $table->tinyInteger('limit')->default(20);
            $table->string('range')->nullable();
            $table->string('selector')->nullable();

            $table->foreign('crawler_id')
                ->references('id')->on('cms_crawlers')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_crawler_list_rule');
    }
}
