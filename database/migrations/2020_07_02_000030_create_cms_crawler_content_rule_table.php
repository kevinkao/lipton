<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCmsCrawlerContentRuleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_crawler_content_rule', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('crawler_id');
            $table->string('column')->nullable();
            $table->string('selector')->nullable();
            $table->string('type')->nullable();
            $table->string('filter')->nullable();

            $table->foreign('crawler_id')
                ->references('id')->on('cms_crawlers')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_crawler_content_rule');
    }
}
