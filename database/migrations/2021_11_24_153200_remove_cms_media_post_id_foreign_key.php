<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveCmsMediaPostIdForeignKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cms_media', function (Blueprint $table) {
            $table->dropForeign('cms_media_post_id_foreign');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cms_media', function (Blueprint $table) {
            $table->foreign('post_id')
                ->references('id')->on('cms_posts');
        });
    }
}
