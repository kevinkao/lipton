<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCmsPostCommentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_post_comment', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('cms_post_id');
            $table->unsignedBigInteger('author_id')->nullable();
            $table->string("title")->nullable();
            $table->text("content");
            $table->tinyInteger("status")->default(0)->comment("0: 審核中, 1: 審核通過");
            $table->timestamps();

            $table->foreign('cms_post_id')
                ->references('id')->on('cms_posts')
                ->onDelete('cascade');
            $table->foreign('author_id')
                ->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_post_comment');
    }
}
