<?php

use Illuminate\Database\Seeder;

class LiptonUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminId = DB::table('users')->insertGetId([
            'name' => 'admin',
            'email' => 'admin@admin.com',
            'password' => bcrypt('123456')
        ]);
        $superAdminId = DB::table('roles')->insertGetId([
            'name' => 'super_admin',
            'display_name' => 'lipton::role.display.super_admin'
        ]);
        $adminRoleId = DB::table('roles')->insertGetId([
            'name' => 'admin',
            'display_name' => 'lipton::role.display.admin'
        ]);
        DB::table('roles')->insertGetId([
            'name' => 'user',
            'display_name' => 'lipton::role.display.general'
        ]);
        DB::table('user_roles')->insert([
            'user_id' => $adminId,
            'role_id' => $superAdminId
        ]);
        DB::table('permission_role')->insert([
            [
                'role_id' => $adminRoleId,
                'permission_id' =>DB::table('permissions')
                    ->insertGetId([ 'key' => 'role_create', 'display_name' => 'lipton::permission.role_create' ])
            ],
            [
                'role_id' => $adminRoleId,
                'permission_id' => DB::table('permissions')
                    ->insertGetId([ 'key' => 'role_delete', 'display_name' => 'lipton::permission.role_delete'  ])
            ],
            [
                'role_id' => $adminRoleId,
                'permission_id' => DB::table('permissions')
                    ->insertGetId([ 'key' => 'role_edit', 'display_name' => 'lipton::permission.role_edit'  ])
            ],
            [
                'role_id' => $adminRoleId,
                'permission_id' => DB::table('permissions')
                    ->insertGetId([ 'key' => 'role_browse', 'display_name' => 'lipton::permission.role_browse'  ])
            ],
            [
                'role_id' => $adminRoleId,
                'permission_id' => DB::table('permissions')
                    ->insertGetId([ 'key' => 'user_create', 'display_name' => 'lipton::permission.user_create'  ])
            ],
            [
                'role_id' => $adminRoleId,
                'permission_id' => DB::table('permissions')
                    ->insertGetId([ 'key' => 'user_delete', 'display_name' => 'lipton::permission.user_delete'  ])
            ],
            [
                'role_id' => $adminRoleId,
                'permission_id' => DB::table('permissions')
                    ->insertGetId([ 'key' => 'user_edit', 'display_name' => 'lipton::permission.user_edit'  ])
            ],
            [
                'role_id' => $adminRoleId,
                'permission_id' => DB::table('permissions')
                    ->insertGetId([ 'key' => 'user_browse', 'display_name' => 'lipton::permission.user_browse'  ])
            ],
            [
                'role_id' => $adminRoleId,
                'permission_id' => DB::table('permissions')
                    ->insertGetId([ 'key' => 'settings_create', 'display_name' => 'lipton::permission.settings_create'  ])
            ],
            [
                'role_id' => $adminRoleId,
                'permission_id' => DB::table('permissions')
                    ->insertGetId([ 'key' => 'settings_delete', 'display_name' => 'lipton::permission.settings_delete'  ])
            ],
            [
                'role_id' => $adminRoleId,
                'permission_id' => DB::table('permissions')
                    ->insertGetId([ 'key' => 'settings_edit', 'display_name' => 'lipton::permission.settings_edit'  ])
            ],
            [
                'role_id' => $adminRoleId,
                'permission_id' => DB::table('permissions')
                    ->insertGetId([ 'key' => 'settings_browse', 'display_name' => 'lipton::permission.settings_browse'  ])
            ],
        ]);
    }
}
