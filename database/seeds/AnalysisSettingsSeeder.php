<?php

use Illuminate\Database\Seeder;

class AnalysisSettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert([
            [
                'key' => 'site_analysis_code',
                'display_name' => 'lipton::settings.description.site_analysis_code',
                'value' => '',
                'type' => 'textarea',
            ],
        ]);
    }
}