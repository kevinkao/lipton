<?php

use Illuminate\Database\Seeder;

class LiptonCmsCommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->insert([
            [ 'key' => 'cms_comment_create', 'display_name' => 'lipton::permission.cms_comment_create' ],
            [ 'key' => 'cms_comment_delete', 'display_name' => 'lipton::permission.cms_comment_delete' ],
            [ 'key' => 'cms_comment_edit', 'display_name' => 'lipton::permission.cms_comment_edit' ],
            [ 'key' => 'cms_comment_browse', 'display_name' => 'lipton::permission.cms_comment_browse' ],
        ]);
    }
}