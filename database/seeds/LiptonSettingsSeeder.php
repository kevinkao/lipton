<?php

use Illuminate\Database\Seeder;

class LiptonSettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert([
            [
                'key' => 'site_title',
                'display_name' => 'lipton::settings.description.site_title',
                'value' => 'Site Name',
                'type' => 'text',
            ],
            [
                'key' => 'site_description',
                'display_name' => 'lipton::settings.description.site_description',
                'value' => 'Site Description',
                'type' => 'text',
            ],
            [
                'key' => 'site_keyword',
                'display_name' => 'lipton::settings.description.site_keyword',
                'value' => 'Site Keywords',
                'type' => 'text',
            ],
            // [
            //     'key' => 'test_textarea',
            //     'display_name' => 'test_textarea',
            //     'value' => '',
            //     'type' => 'textarea',
            // ],
            // [
            //     'key' => 'test_datetime_picker',
            //     'display_name' => 'test_datetime_picker',
            //     'value' => '',
            //     'type' => 'datetime_picker',
            // ],
            // [
            //     'key' => 'test_editor',
            //     'display_name' => 'test_editor',
            //     'value' => '',
            //     'type' => 'editor',
            // ],
        ]);
    }
}