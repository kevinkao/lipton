<?php

use Illuminate\Database\Seeder;

class LiptonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(LiptonUserSeeder::class);
        $this->call(LiptonSettingsSeeder::class);
        $this->call(LiptonCmsSeeder::class);
        $this->call(LiptonCmsCrawlerSeeder::class);
        $this->call(AnalysisSettingsSeeder::class);
        $this->call(LiptonCmsCommentSeeder::class);
    }
}