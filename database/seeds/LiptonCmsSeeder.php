<?php

use Illuminate\Database\Seeder;

class LiptonCmsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->insert([
            [ 'key' => 'cms_category_create', 'display_name' => 'lipton::permission.cms_category_create' ],
            [ 'key' => 'cms_category_delete', 'display_name' => 'lipton::permission.cms_category_delete' ],
            [ 'key' => 'cms_category_edit', 'display_name' => 'lipton::permission.cms_category_edit' ],
            [ 'key' => 'cms_category_browse', 'display_name' => 'lipton::permission.cms_category_browse' ],

            [ 'key' => 'cms_post_create', 'display_name' => 'lipton::permission.cms_post_create' ],
            [ 'key' => 'cms_post_delete', 'display_name' => 'lipton::permission.cms_post_delete' ],
            [ 'key' => 'cms_post_edit', 'display_name' => 'lipton::permission.cms_post_edit' ],
            [ 'key' => 'cms_post_browse', 'display_name' => 'lipton::permission.cms_post_browse' ],

            [ 'key' => 'cms_crawler_create', 'display_name' => 'lipton::permission.cms_crawler_create' ],
            [ 'key' => 'cms_crawler_delete', 'display_name' => 'lipton::permission.cms_crawler_delete' ],
            [ 'key' => 'cms_crawler_edit', 'display_name' => 'lipton::permission.cms_crawler_edit' ],
            [ 'key' => 'cms_crawler_browse', 'display_name' => 'lipton::permission.cms_crawler_browse' ],
        ]);
    }
}