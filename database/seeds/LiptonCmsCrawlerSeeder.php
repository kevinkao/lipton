<?php

use Illuminate\Database\Seeder;

class LiptonCmsCrawlerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cms_crawlers')->insert([
            ['id' => 2, 'title' => '新浪国际足球'],
            ['id' => 5, 'title' => '新浪中超'],
            ['id' => 6, 'title' => '新浪CBA'],
            ['id' => 7, 'title' => '新浪歐冠'],
            ['id' => 8, 'title' => '新浪英超'],
            ['id' => 9, 'title' => '新浪西甲'],
            ['id' => 10, 'title' => '新浪德甲'],
            ['id' => 11, 'title' => '新浪法甲'],
            ['id' => 12, 'title' => '新浪意甲'],
            ['id' => 13, 'title' => '新浪乒乓球'],
            ['id' => 14, 'title' => '搜狐英超'],
            ['id' => 15, 'title' => '搜狐中超'],
            ['id' => 16, 'title' => '搜狐西甲'],
            ['id' => 17, 'title' => '搜狐德甲'],
            ['id' => 18, 'title' => '搜狐法甲'],
            ['id' => 19, 'title' => '搜狐意甲'],
        ]);
        DB::table('cms_crawler_schedule')->insert([
            ['crawler_id' => 2, 'method' => 1],
            ['crawler_id' => 5, 'method' => 1],
            ['crawler_id' => 6, 'method' => 1],
            ['crawler_id' => 7, 'method' => 1],
            ['crawler_id' => 8, 'method' => 1],
            ['crawler_id' => 9, 'method' => 1],
            ['crawler_id' => 10, 'method' => 1],
            ['crawler_id' => 11, 'method' => 1],
            ['crawler_id' => 12, 'method' => 1],
            ['crawler_id' => 13, 'method' => 1],
            ['crawler_id' => 14, 'method' => 1],
            ['crawler_id' => 15, 'method' => 1],
            ['crawler_id' => 16, 'method' => 1],
            ['crawler_id' => 17, 'method' => 1],
            ['crawler_id' => 18, 'method' => 1],
            ['crawler_id' => 19, 'method' => 1],
        ]);
        DB::table('cms_crawler_list_rule')->insert([
            [ 'crawler_id' => 2, 'start_url' => 'http://sports.sina.com.cn/global/', 'range' => '.blk2 li', 'selector' => 'a' ],
            [ 'crawler_id' => 5, 'start_url' => 'http://sports.sina.com.cn/csl/', 'range' => '.blk13 li:not(.first)', 'selector' => 'a' ],
            [ 'crawler_id' => 6, 'start_url' => 'http://sports.sina.com.cn/cba/', 'range' => '.news-list-b p', 'selector' => 'a' ],
            [ 'crawler_id' => 7, 'start_url' => 'http://sports.sina.com.cn/global/', 'range' => '.ul-type1 li', 'selector' => 'a' ],
            [ 'crawler_id' => 8, 'start_url' => 'https://sports.sina.com.cn/g/premierleague/', 'range' => '.match_blk_rt:first ul li', 'selector' => 'a' ],
            [ 'crawler_id' => 9, 'start_url' => 'https://sports.sina.com.cn/g/laliga/', 'range' => '.match_blk_rt:first ul li', 'selector' => 'a' ],
            [ 'crawler_id' => 10, 'start_url' => 'https://sports.sina.com.cn/g/bundesliga/', 'range' => '.blk01 ul:first li', 'selector' => 'a' ],
            [ 'crawler_id' => 11, 'start_url' => 'http://sports.sina.com.cn/g/ligue1/', 'range' => '.ul01 li', 'selector' => 'a' ],
            [ 'crawler_id' => 12, 'start_url' => 'http://sports.sina.com.cn/g/seriea/', 'range' => '.match_news_list:first li', 'selector' => 'a' ],
            [ 'crawler_id' => 13, 'start_url' => 'https://sports.sina.com.cn/others/pingpang.shtml', 'range' => '.main1M:first .list2 li', 'selector' => 'a' ],
            [ 'crawler_id' => 14, 'start_url' => 'https://sports.sohu.com/s/premierleague?spm=smpc.fb-sports-home.top-subnav.9.1618559149426SuV6Qjk', 'range' => '[data-spm=top-news-1] li', 'selector' => 'a' ],
            [ 'crawler_id' => 15, 'start_url' => 'https://sports.sohu.com/s/cnmenfootball', 'range' => '.sports-head-line li', 'selector' => 'a' ],
            [ 'crawler_id' => 16, 'start_url' => 'https://sports.sohu.com/s/laliga', 'range' => '.sports-head-line li', 'selector' => 'a' ],
            [ 'crawler_id' => 17, 'start_url' => 'https://sports.sohu.com/s/bundesliga?spm=smpc.fb-xj-home.top-subnav.6.1618565349749f4r0dyl', 'range' => '[data-spm=top-news-1] li', 'selector' => 'a' ],
            [ 'crawler_id' => 18, 'start_url' => 'https://sports.sohu.com/s/ligue1?spm=smpc.fb-xj-home.top-subnav.7.1618565349749f4r0dyl', 'range' => '[data-spm=top-news-1] li', 'selector' => 'a' ],
            [ 'crawler_id' => 19, 'start_url' => 'https://sports.sohu.com/s/seriea?spm=smpc.fb-fj-home.top-subnav.6.16185663703118t1pmq1', 'range' => '[data-spm=top-news-1] li', 'selector' => 'a' ],
        ]);
        DB::table('cms_crawler_content_rule')->insert([
            [ 'crawler_id' => 2, 'column' => 'title', 'selector' => 'h1.main-title', 'type' => 'text', 'filter' => null ],
            [ 'crawler_id' => 2, 'column' => 'content', 'selector' => '#artibody', 'type' => 'html', 'filter' => '-#left_hzh_ad -p:last' ],
            [ 'crawler_id' => 2, 'column' => 'cover', 'selector' => '.img_wrapper:first img', 'type' => 'src', 'filter' => null ],

            [ 'crawler_id' => 5, 'column' => 'title', 'selector' => 'h1.main-title', 'type' => 'text', 'filter' => null ],
            [ 'crawler_id' => 5, 'column' => 'content', 'selector' => '#artibody', 'type' => 'html', 'filter' => '-#left_hzh_ad -p:last' ],
            [ 'crawler_id' => 5, 'column' => 'cover', 'selector' => '.img_wrapper:first img', 'type' => 'src', 'filter' => null ],

            [ 'crawler_id' => 6, 'column' => 'title', 'selector' => 'h1.main-title', 'type' => 'text', 'filter' => null ],
            [ 'crawler_id' => 6, 'column' => 'content', 'selector' => '#artibody', 'type' => 'html', 'filter' => '-#left_hzh_ad -p:last' ],
            [ 'crawler_id' => 6, 'column' => 'cover', 'selector' => '#artibody .img_wrapper > img', 'type' => 'src', 'filter' => null ],

            [ 'crawler_id' => 7, 'column' => 'title', 'selector' => 'h1.main-title', 'type' => 'text', 'filter' => null ],
            [ 'crawler_id' => 7, 'column' => 'content', 'selector' => '#artibody', 'type' => 'html', 'filter' => '-script -#left_hzh_ad -p:last' ],
            [ 'crawler_id' => 7, 'column' => 'cover', 'selector' => '#artibody img:first', 'type' => 'src', 'filter' => null ],

            [ 'crawler_id' => 8, 'column' => 'title', 'selector' => 'h1.main-title', 'type' => 'text', 'filter' => null ],
            [ 'crawler_id' => 8, 'column' => 'content', 'selector' => '#artibody', 'type' => 'html', 'filter' => '-#left_hzh_ad -p:last -.show_statement' ],
            [ 'crawler_id' => 8, 'column' => 'cover', 'selector' => '#artibody img:first', 'type' => 'src', 'filter' => null ],

            [ 'crawler_id' => 9, 'column' => 'title', 'selector' => 'h1.main-title', 'type' => 'text', 'filter' => null ],
            [ 'crawler_id' => 9, 'column' => 'content', 'selector' => '#artibody', 'type' => 'html', 'filter' => '-#left_hzh_ad -p:last -.show_statement' ],
            [ 'crawler_id' => 9, 'column' => 'cover', 'selector' => '#artibody img:first', 'type' => 'src', 'filter' => null ],

            [ 'crawler_id' => 10, 'column' => 'title', 'selector' => 'h1.main-title', 'type' => 'text', 'filter' => null ],
            [ 'crawler_id' => 10, 'column' => 'content', 'selector' => '#artibody', 'type' => 'html', 'filter' => '-#left_hzh_ad -p:last -.show_statement' ],
            [ 'crawler_id' => 10, 'column' => 'cover', 'selector' => '#artibody img:first', 'type' => 'src', 'filter' => null ],

            [ 'crawler_id' => 11, 'column' => 'title', 'selector' => 'h1.main-title', 'type' => 'text', 'filter' => null ],
            [ 'crawler_id' => 11, 'column' => 'content', 'selector' => '#artibody', 'type' => 'html', 'filter' => '-#left_hzh_ad -p:last -.show_statement' ],
            [ 'crawler_id' => 11, 'column' => 'cover', 'selector' => '#artibody img:first', 'type' => 'src', 'filter' => null ],

            [ 'crawler_id' => 12, 'column' => 'title', 'selector' => 'h1.main-title', 'type' => 'text', 'filter' => null ],
            [ 'crawler_id' => 12, 'column' => 'content', 'selector' => '#artibody', 'type' => 'html', 'filter' => '-#left_hzh_ad -p:last -.show_statement' ],
            [ 'crawler_id' => 12, 'column' => 'cover', 'selector' => '#artibody img:first', 'type' => 'src', 'filter' => null ],

            [ 'crawler_id' => 13, 'column' => 'title', 'selector' => 'h1.main-title', 'type' => 'text', 'filter' => null ],
            [ 'crawler_id' => 13, 'column' => 'content', 'selector' => '#artibody', 'type' => 'html', 'filter' => '-#left_hzh_ad -p:last -.show_statement' ],
            [ 'crawler_id' => 13, 'column' => 'cover', 'selector' => '#artibody img:first', 'type' => 'src', 'filter' => null ],

            [ 'crawler_id' => 14, 'column' => 'title', 'selector' => '.title-info-title', 'type' => 'text', 'filter' => null ],
            [ 'crawler_id' => 14, 'column' => 'content', 'selector' => '#mp-editor', 'type' => 'html', 'filter' => '-.statement -.backword' ],
            [ 'crawler_id' => 14, 'column' => 'cover', 'selector' => '#mp-editor img:first', 'type' => 'src', 'filter' => null ],

            [ 'crawler_id' => 15, 'column' => 'title', 'selector' => 'h1', 'type' => 'text', 'filter' => null ],
            [ 'crawler_id' => 15, 'column' => 'content', 'selector' => '#mp-editor', 'type' => 'html', 'filter' => '-.statement -.backword' ],
            [ 'crawler_id' => 15, 'column' => 'cover', 'selector' => '#mp-editor img:first', 'type' => 'src', 'filter' => null ],

            [ 'crawler_id' => 16, 'column' => 'title', 'selector' => 'h1', 'type' => 'text', 'filter' => null ],
            [ 'crawler_id' => 16, 'column' => 'content', 'selector' => '#mp-editor', 'type' => 'html', 'filter' => '-.statement -.backword' ],
            [ 'crawler_id' => 16, 'column' => 'cover', 'selector' => '#mp-editor img:first', 'type' => 'src', 'filter' => null ],

            [ 'crawler_id' => 17, 'column' => 'title', 'selector' => '.title-info-title', 'type' => 'text', 'filter' => null ],
            [ 'crawler_id' => 17, 'column' => 'content', 'selector' => '#mp-editor', 'type' => 'html', 'filter' => '-.statement -.backword' ],
            [ 'crawler_id' => 17, 'column' => 'cover', 'selector' => '#mp-editor img:first', 'type' => 'src', 'filter' => null ],

            [ 'crawler_id' => 18, 'column' => 'title', 'selector' => '.title-info-title', 'type' => 'text', 'filter' => null ],
            [ 'crawler_id' => 18, 'column' => 'content', 'selector' => '#mp-editor', 'type' => 'html', 'filter' => '-.statement -.backword' ],
            [ 'crawler_id' => 18, 'column' => 'cover', 'selector' => '#mp-editor img:first', 'type' => 'src', 'filter' => null ],

            [ 'crawler_id' => 19, 'column' => 'title', 'selector' => '.title-info-title', 'type' => 'text', 'filter' => null ],
            [ 'crawler_id' => 19, 'column' => 'content', 'selector' => '#mp-editor', 'type' => 'html', 'filter' => '-.statement -.backword' ],
            [ 'crawler_id' => 19, 'column' => 'cover', 'selector' => '#mp-editor img:first', 'type' => 'src', 'filter' => null ],
        ]);
    }
}