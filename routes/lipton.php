<?php

$prefix = config('lipton.admin_prefix');

Route::group(['as' => 'lipton.', 'prefix' => $prefix, 'middleware' => ['web']], function () {

    $namespacePrefix = '\\KevinKao\\Lipton\\Http\\Controllers';

    Route::get('/login', "{$namespacePrefix}\AuthController@index")->name('login');
    Route::post('/login', "{$namespacePrefix}\AuthController@login")->name('post.login');

    Route::group(['middleware' => ['admin.user']], function () use ($namespacePrefix) {

        Route::get('logout', "{$namespacePrefix}\AuthController@logout")->name('logout');

        Route::get('/', "{$namespacePrefix}\IndexController@index")->name('index');

        Route::get('/user/auth', "{$namespacePrefix}\UserController@showAuth")->name('user.showAuth');
        Route::get('/user', "{$namespacePrefix}\UserController@index")->name('user.index');
        Route::post('/user/datatable', "{$namespacePrefix}\UserController@datatable")->name('user.datatable');
        Route::post('/user', "{$namespacePrefix}\UserController@store")->name('user.store');
        Route::put('/user/{id}', "{$namespacePrefix}\UserController@update")->name('user.update');
        Route::delete('/user/{id}', "{$namespacePrefix}\UserController@destroy")->name('user.destroy');

        Route::get('/role', "{$namespacePrefix}\RoleController@index")->name('role.index');
        Route::post('/role/datatable', "{$namespacePrefix}\RoleController@datatable")->name('role.datatable');
        Route::post('/role', "{$namespacePrefix}\RoleController@store")->name('role.store');
        Route::put('/role/{id}', "{$namespacePrefix}\RoleController@update")->name('role.update');
        Route::delete('/role/{id}', "{$namespacePrefix}\RoleController@destroy")->name('role.destroy');

        Route::get('/settings', "{$namespacePrefix}\SettingsController@index")->name('settings.index');
        Route::put('/settings', "{$namespacePrefix}\SettingsController@update")->name('settings.update');
        Route::post('/settings/upload', "{$namespacePrefix}\SettingsController@upload")->name('settings.upload');

        Route::group(["as" => 'cms.', "prefix" => "cms"], function() use ($namespacePrefix) {
            Route::get('/category', "{$namespacePrefix}\CMS\CategoryController@index")->name("category.index");
            Route::post('/category/datatable', "{$namespacePrefix}\CMS\CategoryController@datatable")->name("category.datatable");
            Route::post('/category', "{$namespacePrefix}\CMS\CategoryController@store")->name('category.store');
            Route::put('/category/{id}', "{$namespacePrefix}\CMS\CategoryController@update")->name('category.update');
            Route::put('/category/{id}/visible', "{$namespacePrefix}\CMS\CategoryController@visibleToggle")->name('category.visible.toggle');
            Route::delete('/category/{id}', "{$namespacePrefix}\CMS\CategoryController@destroy")->name('category.destroy');
            Route::get('/category/exclude/{id}', "{$namespacePrefix}\CMS\CategoryController@exclude")->name('category.index.exclude');

            Route::get('/post', "{$namespacePrefix}\CMS\PostController@index")->name("post.index");
            Route::get('/post/create', "{$namespacePrefix}\CMS\PostController@create")->name("post.create");
            Route::post('/post', "{$namespacePrefix}\CMS\PostController@store")->name("post.store");
            Route::post('/post/editor/upload', "{$namespacePrefix}\CMS\PostController@editorUpload")->name('post.editorUpload');
            Route::match(['post', 'get'], '/post/datatable', "{$namespacePrefix}\CMS\PostController@datatable")->name('post.datatable');
            Route::get('/post/{id}/edit', "{$namespacePrefix}\CMS\PostController@edit")->name('post.edit');
            Route::delete('/post/{id}', "{$namespacePrefix}\CMS\PostController@destroy")->name('post.destroy');
            Route::post('/post/{id}', "{$namespacePrefix}\CMS\PostController@update")->name('post.update');

            Route::get('/comment', "{$namespacePrefix}\CMS\CommentController@index")->name("comment.index");
            Route::post('/comment/datatable', "{$namespacePrefix}\CMS\CommentController@datatable")->name("comment.datatable");
            Route::put('/comment/{id}/check', "{$namespacePrefix}\CMS\CommentController@checkToggle")->name('comment.status.toggle');
            Route::delete('/comment/{id}', "{$namespacePrefix}\CMS\CommentController@destroy")->name('comment.destroy');

            Route::get('/crawler', "{$namespacePrefix}\CMS\CrawlerController@index")->name("crawler.index");
            Route::post('/crawler', "{$namespacePrefix}\CMS\CrawlerController@store")->name("crawler.store");
            Route::match(['post', 'get'], '/crawler/datatable', "{$namespacePrefix}\CMS\CrawlerController@datatable")->name('crawler.datatable');
            Route::put('/crawler/{id}', "{$namespacePrefix}\CMS\CrawlerController@update")->name('crawler.update');
            Route::put('/crawler/{id}/operation', "{$namespacePrefix}\CMS\CrawlerController@operation")->name('crawler.operation');
            Route::delete('/crawler/{id}', "{$namespacePrefix}\CMS\CrawlerController@destroy")->name('crawler.destroy');
            Route::get('/crawler/{id}/schedule', "{$namespacePrefix}\CMS\CrawlerController@getSchedule")->name('crawler.schedule.show');
            Route::put('/crawler/{id}/schedule', "{$namespacePrefix}\CMS\CrawlerController@updateSchedule")->name('crawler.schedule.update');
            Route::put('/crawler/{id}/rules', "{$namespacePrefix}\CMS\CrawlerController@updateRules")->name('crawler.rules.update');
            Route::get('/crawler/{id}/rules', "{$namespacePrefix}\CMS\CrawlerController@rules")->name('crawler.rules.index');
            Route::post('/crawler/test/list', "{$namespacePrefix}\CMS\CrawlerController@testListUrl")->name('crawler.list.test');
            Route::post('/crawler/test/content', "{$namespacePrefix}\CMS\CrawlerController@testContentUrl")->name('crawler.content.test');
            Route::get('/crawler/log', "{$namespacePrefix}\CMS\CrawlerController@log")->name('crawler.log');
            Route::post('/crawler/{id}/trigger', "{$namespacePrefix}\CMS\CrawlerController@trigger")->name('crawler.trigger');
        });
    });
});