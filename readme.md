## Installation

Update composer.json like below example first.
```
"license": "MIT",
"type": "project",
"repositories": [{
    "type": "vcs",
    "url": "https://gitlab.com/kevinkao/lipton.git"
}],
```

And then, update composer packages.

```
$ composer require kevinkao/lipton
```

Now you already have ffadmin package! But we need to publish some files and configure to laravel structure. Let's do it below.(Of course you need to switch to laravel work folder.)
```
$ php artisan vendor:publish
  Which provider or tag's files would you like to publish?:
  [0 ] Publish files from all providers and tags listed below
  [1 ] Provider: Fideloper\Proxy\TrustedProxyServiceProvider
  [2 ] Provider: Illuminate\Foundation\Providers\FoundationServiceProvider
  [3 ] Provider: Illuminate\Mail\MailServiceProvider
  [4 ] Provider: Illuminate\Notifications\NotificationServiceProvider
  [5 ] Provider: Illuminate\Pagination\PaginationServiceProvider
  [6 ] Provider: KevinKao\Lipton\Providers\LiptonServiceProvider
  [7 ] Provider: Laravel\Tinker\TinkerServiceProvider
  [8 ] Tag: laravel-errors
  [9 ] Tag: laravel-mail
  [10] Tag: laravel-notifications
  [11] Tag: laravel-pagination
  [12] Tag: lipton_assets
  [13] Tag: lipton_config
  [14] Tag: lipton_seeds
 > 6
```

```
$ php artisan migrate
$ php artisan db:seed --class LiptonSeeder
```

## How to create new nav item

\App\NavItems\ExampleNavItem.php
``` php
<?php

namespace \App\NavItems;

use \KevinKao\NavItems\NavItem;
use Gate;

/**
* 用戶查詢
*/
class ExampleNavItem extends NavItem
{
    // 這裡會直接代入locale, 如果不用語系直接填入中文也沒差
    public $title = 'example.nav.index';
    public $iconClass = 'fa-users'; // icon
    public $hasTreeView = false; // 是否有下拉選項

    public function getLink($fullUrl = true)
    {
        // 初始化側邊欄時, 會訪問此函式並取得實際點擊時的連結
        return route('lipton.example.index', [], $fullUrl);
    }

    public function isVisible()
    {
        // 初始化側邊欄時, 會訪問此函式, 在這邊可以做判斷是否顯示該項目
        return Gate::allows('browse', 'example');
    }
}
```

### 安裝OpenCC

```
apt-get install doxygen
```

```
git clone https://github.com/BYVoid/OpenCC.git --depth 1
cd OpenCC
make
sudo make install
```
